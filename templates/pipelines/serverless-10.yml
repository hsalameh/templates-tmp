# See https://yottadv.atlassian.net/wiki/spaces/DEV/pages/2074083587/Gitlab+Serverless+Pipeline
# for documentation for this pipeline
include:
  - project: fabric2-public/cicd/cicd-templates
    ref: main
    file:
      - templates/sls.yml
      - templates/postman.yml
      - templates/code_quality.yml
      - templates/a11y.yml
      - templates/npm.yml
      - templates/sast.yml
      - templates/secret_detection.yml
      - templates/browser_performance.yml
      - templates/platform.yml
      - templates/dependency-check.yml

variables:
  # Defaults for commerce-client-deploy job
  TARGET_API_NAME: empty
  REQ_FUNCTION_SET: empty
  
  # Feature flags and cache configs
  FF_SCRIPT_SECTIONS: "true"
  FF_USE_FASTZIP: "true"
  ARTIFACT_COMPRESSION_LEVEL: default # can also be set to fastest, fast, slow and slowest. If just enabling fastzip is not enough try setting this to fastest or fast.
  CACHE_COMPRESSION_LEVEL: default # same as above, but for caches
  TRANSFER_METER_FREQUENCY: 5s # will display transfer progress every 5 seconds for artifacts and remote caches.

  # BUILD_ID can be modified from $CI_COMMIT_SHORT_SHA to a previous commit SHA, in which case
  # the pipeline will fetch the previous build from the registry rather than rebuild.
  # Useful mostly in debugging situations for quick feedback loops.
  BUILD_ID: $CI_COMMIT_SHORT_SHA
  DEV_ENV_NAME: DEV
  QA_ENV_NAME: QA
  INT_UAT_ENV_NAME: INT-UAT
  SANDBOX_ENV_NAME: SANDBOX
  PROD_ENV_NAME: PROD


  DEV_COPILOT_STAGE: dev02
  DEV_COMMERCE_STAGE: dev01
  DEV_COPILOT_CLIENT: greatwall
  DEV_COMMERCE_CLIENT: bailey

  QA_COPILOT_STAGE: stg02
  QA_COMMERCE_STAGE: stg02
  QA_COPILOT_CLIENT: greatwall
  QA_COMMERCE_CLIENT: bailey

  INT_UAT_COPILOT_STAGE: prod02
  INT_UAT_COMMERCE_STAGE: prod02
  INT_UAT_COMMERCE_CLIENT: bailey-prod02
  INT_UAT_COPILOT_CLIENT: greatwall-production

  SANDBOX_COPILOT_STAGE: sandbox
  SANDBOX_COMMERCE_STAGE: sandbox
  SANDBOX_COPILOT_CLIENT: greatwall-sandbox
  BETA_SANDBOX_COPILOT_STAGE: betasandbox

  BETA_SANDBOX_DR_STAGE: sandbox
  BETA_SANDBOX_DR_CLIENT: betasandbox-dr

  PROD_COPILOT_STAGE: prod01
  PROD_COMMERCE_STAGE: prod01
  PROD_COPILOT_CLIENT: greatwall-production

  # Serverless variables
  WARMUP_ENABLE: "true"
  SLS_DEBUG: "*"

stages:
- dependencies
- validate
- build
- test
- deploy-DEV
- test-DEV
- deploy-QA
- test-QA
- deploy-INT-UAT
- test-INT-UAT
- sandbox-approve
- prepare-SANDBOX
- deploy-SANDBOX
- prepare-DEMO
- deploy-DEMO
- production-approve
- prepare-PROD
- deploy-PROD
- deploy-Selective

.rules-anchors:
  rules:
    - &run-on-trigger
      if: '$CI_PIPELINE_SOURCE == "trigger"'
      when: always
    - &merge-request
      if: $CI_MERGE_REQUEST_ID
    - &main-or-merge-request
      if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_MERGE_REQUEST_ID'
    - &main-branch
      if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: manual
    - &env-dump
      if: $ENABLE_ENV_DUMP != "true"
      when: never
    - &skip-dependency-check
      if: $SKIP_DEP_CHECK  || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-lint
      if: $SKIP_LINT || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-tests
      if: $SKIP_UNIT_TEST || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-codequality
      if: $SKIP_CODE_QUALITY || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-sonarcloud
      if: $SKIP_SONARCLOUD || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-semgrep
      if: $SKIP_SEMGREP || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-secretscanner
      if: $SKIP_SECRET_SCANNER || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-a11y
      if: $A11Y_URL == null || $A11Y_URL == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-perf-test
      if: $BROWSER_PERF_URL == null || $BROWSER_PERF_URL == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never

    - &skip-dev-copilot-stage
      if: $DEV_COPILOT_STAGE == null || $DEV_COPILOT_STAGE == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-dev-commerce-stage
      if:  $DEV_COMMERCE_STAGE == null || $DEV_COMMERCE_STAGE == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-qa-commerce-stage
      if: $QA_COMMERCE_STAGE == null || $QA_COMMERCE_STAGE == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-qa-copilot-stage
      if: $QA_COPILOT_STAGE == null || $QA_COPILOT_STAGE == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-intuat-commerce-stage
      if: $INT_UAT_COMMERCE_STAGE == null || $INT_UAT_COMMERCE_STAGE == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-intuat-copilot-stage
      if: $INT_UAT_COPILOT_STAGE == null || $INT_UAT_COPILOT_STAGE == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-sandbox-commerce-stage
      if: $SANDBOX_COMMERCE_STAGE == null || $SANDBOX_COMMERCE_STAGE == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-sandbox-copilot-stage
      if: $SANDBOX_COPILOT_STAGE == null || $SANDBOX_COPILOT_STAGE == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-beta-copilot-stage
      if: $BETA_SANDBOX_COPILOT_STAGE == null || $BETA_SANDBOX_COPILOT_STAGE == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-all-sandbox
      if: ($SANDBOX_COMMERCE_STAGE == null || $SANDBOX_COMMERCE_STAGE == "") && ($SANDBOX_COPILOT_STAGE == null || $SANDBOX_COPILOT_STAGE == "") || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-prod-commerce-stage
      if: $PROD_COMMERCE_STAGE == null || $PROD_COMMERCE_STAGE == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-prod-copilot-stage
      if: $PROD_COPILOT_STAGE == null || $PROD_COPILOT_STAGE == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-all-prod
      if: ($PROD_COMMERCE_STAGE == null || $PROD_COMMERCE_STAGE == "") && ($PROD_COPILOT_STAGE == null || $PROD_COPILOT_STAGE == "")  || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-postman
      if: $POSTMAN_API_KEY == null || $POSTMAN_API_KEY == "" || $POSTMAN_COLLECTION_UUID == null || $POSTMAN_COLLECTION_UUID == "" || $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - &skip-dev-postman
      if: $POSTMAN_DEV02_ENVIRONMENT_UUID == null || $POSTMAN_DEV02_ENVIRONMENT_UUID == ""
      when: never
    - &skip-qa-postman
      if: $POSTMAN_STG02_ENVIRONMENT_UUID == null || $POSTMAN_STG02_ENVIRONMENT_UUID == ""
      when: never
    - &skip-intuat-postman
      if: $POSTMAN_PROD02_ENVIRONMENT_UUID == null || $POSTMAN_PROD02_ENVIRONMENT_UUID == ""
      when: never
    - &skip-sandbox-postman
      if: $POSTMAN_SANDBOX_ENVIRONMENT_UUID == null || $POSTMAN_SANDBOX_ENVIRONMENT_UUID == ""
      when: never



workflow:
  rules:
    - *main-or-merge-request

envdump:
  image: alpine
  stage: .pre
  script: env
  rules:
    - *env-dump
    - *main-or-merge-request

lint:
  stage: validate
  extends:
    - .npm-run-10
  variables:
    npm_cmd: $NPM_RUN_LINT_CMD
  needs: []
  allow_failure: true
  rules:
    - *skip-lint
    - *main-or-merge-request

unit-test:
  stage: validate
  extends:
    - .npm-run-10
  variables:
    npm_cmd: $NPM_RUN_UNIT_TEST_CMD
  needs: []
  allow_failure: true
  rules:
    - *skip-tests
    - *main-or-merge-request

build:
  stage: build
  extends:
    - .sls-webpack-10
  variables:
    STAGE: build
    SLS_DEBUG: '*'
  dependencies: []
  needs:
    - job: lint
      optional: true
    - job: unit-test
      optional: true
  rules:
    - *main-or-merge-request

.base-test:
  stage: test
  needs: [build]
  dependencies: []

dependency-check:
  extends:
    - .base-test
    - .dependency-check
  rules:
    - *skip-dependency-check
    - *main-or-merge-request

sonarcloud:
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  extends:
    - .base-test
  allow_failure: true
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
  rules:
    - *skip-sonarcloud
    - *main-or-merge-request
  needs: [build, unit-test]
  dependencies: [unit-test]

code-quality:
  extends:
    - .code-quality
    - .base-test
  rules:
    - *skip-codequality
    - *main-or-merge-request

secret-scanner:
  extends:
    - .secret_detection_default_branch
    - .base-test
  rules:
    - *skip-secretscanner
    - *main-or-merge-request

semgrep-sast:
  cache: {}
  extends:
    - .semgrep-sast
    - .base-test
  rules:
    - *skip-semgrep
    - *main-or-merge-request

.base-deploy-DEV:
  stage: deploy-DEV
  extends:
    - .sls-deploy-10
  dependencies: []
  needs:
    - job: build
      optional: false
    - job: code-quality
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true

# follows promotion path laid out here
# https://docs.google.com/spreadsheets/d/1r9PPD_V3VyOymucosdmHgmRy8GguQhBjZWmZhKMMKJg
copilot-dev02:
  extends:
    - .base-deploy-DEV
  environment:
    name: $DEV_ENV_NAME/${DEV_COPILOT_STAGE}-copilot
  variables:
    PLATFORM: copilot
    CLIENT: $DEV_COPILOT_CLIENT
    STAGE: $DEV_COPILOT_STAGE
  rules:
    - *skip-dev-copilot-stage
    - *main-or-merge-request

commerce-dev01:
  extends:
    - .base-deploy-DEV
  environment:
    name: $DEV_ENV_NAME/${DEV_COMMERCE_STAGE}-commerce
  variables:
    PLATFORM: commerce
    CLIENT: $DEV_COMMERCE_CLIENT
    STAGE: $DEV_COMMERCE_STAGE
  rules:
    - *skip-dev-commerce-stage
    - *main-or-merge-request

postman-dev02:
  stage: test-DEV
  extends:
    - .postman
  variables:
    POSTMAN_ENVIRONMENT_UUID: $POSTMAN_DEV02_ENVIRONMENT_UUID
  needs:
    - commerce-dev01
    - copilot-dev02
  dependencies: []
  rules:
    - *skip-postman
    - *skip-dev-postman
    - *main-or-merge-request

a11y:
  stage: test-DEV
  extends:
    - .a11y
  variables:
    a11y_url: $A11Y_URL
  needs:
    - commerce-dev01
    - copilot-dev02
  dependencies: []
  rules:
    - *skip-a11y
    - *skip-dev-copilot-stage
    - *skip-dev-commerce-stage
    - *main-or-merge-request

.base-deploy-QA:
  stage: deploy-QA
  extends:
    - .sls-deploy-10
  dependencies: []
  needs:
    - job: build
    - job: code-quality
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - job: a11y
      optional: true
    - job: postman-dev02
      optional: true
    - job: commerce-dev01
      optional: true
    - job: copilot-dev02
      optional: true

copilot-stg02:
  extends:
    - .base-deploy-QA
  environment:
    name: $QA_ENV_NAME/${QA_COPILOT_STAGE}-copilot
  variables:
    PLATFORM: copilot
    CLIENT: $QA_COPILOT_CLIENT
    STAGE: $QA_COPILOT_STAGE
  rules:
    - *skip-qa-copilot-stage
    - *main-or-merge-request

commerce-stg02:
  extends:
    - .base-deploy-QA
  environment:
    name: $QA_ENV_NAME/${QA_COMMERCE_STAGE}-commerce
  variables:
    PLATFORM: commerce
    CLIENT: $QA_COMMERCE_CLIENT
    STAGE: $QA_COMMERCE_STAGE
  rules:
    - *skip-qa-commerce-stage
    - *main-or-merge-request

postman-stg02:
  extends:
    - .postman
  stage: test-QA
  variables:
    POSTMAN_ENVIRONMENT_UUID: $POSTMAN_STG02_ENVIRONMENT_UUID
  needs:
    # For tests, all we need is the deploy jobs that are being tested.
    - commerce-stg02
    - copilot-stg02
  dependencies: []
  rules:
    - *skip-postman
    - *skip-qa-postman
    - *main-or-merge-request

.base-deploy-INT-UAT:
  stage: deploy-INT-UAT
  extends:
    - .sls-deploy-10
  dependencies: []
  needs:
    # Copy stg02s needs
    - build
    - job: code-quality
      optional: true
    - job: dependency-check
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - job: a11y
      optional: true
    - job: postman-dev02
      optional: true
    - job: commerce-dev01
      optional: true
    - job: copilot-dev02
      optional: true
    # Add stg02 jobs.
    - job: postman-stg02
      optional: true
    - job: commerce-stg02
      optional: true
    - job: copilot-stg02
      optional: true

copilot-prod02:
  extends: .base-deploy-INT-UAT
  environment:
    name: $INT_UAT_ENV_NAME/${INT_UAT_COPILOT_STAGE}-copilot
  variables:
    PLATFORM: copilot
    CLIENT: $INT_UAT_COPILOT_CLIENT
    STAGE: $INT_UAT_COPILOT_STAGE
  rules:
    - *skip-intuat-copilot-stage
    - *main-branch

commerce-prod02:
  extends: .base-deploy-INT-UAT
  environment:
    name: $INT_UAT_ENV_NAME/${INT_UAT_COMMERCE_STAGE}-commerce
  variables:
    PLATFORM: commerce
    CLIENT: $INT_UAT_COMMERCE_CLIENT
    STAGE: $INT_UAT_COMMERCE_STAGE
  rules:
    - *skip-intuat-commerce-stage
    - *main-branch

.base-test-INT-UAT:
  stage: test-INT-UAT
  dependencies: []
  needs:
    - build
    - job: code-quality
      optional: true
    - job: dependency-check
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - copilot-prod02
    - commerce-prod02

browser-performance:
  extends:
    - .browser-performance
    - .base-test-INT-UAT
  variables:
    URL: $BROWSER_PERF_URL
  rules:
    - *skip-perf-test
    - *skip-intuat-copilot-stage
    - *skip-intuat-commerce-stage
    - *main-branch

postman-prod02:
  extends:
    - .postman
    - .base-test-INT-UAT
  variables:
    POSTMAN_ENVIRONMENT_UUID: $POSTMAN_PROD02_ENVIRONMENT_UUID
  rules:
    - *skip-postman
    - *skip-intuat-postman
    - *main-branch

approve-sandbox-deploy:
  stage: sandbox-approve
  environment:
    name: sandbox-approve
    action: prepare
  script:
    - echo $GITLAB_USER_NAME approves of this deployment.
  dependencies: []
  rules:
    - *skip-all-sandbox
    - *main-branch
  needs:
    # Copy stg02s needs
    - build
    - job: code-quality
      optional: true
    - job: dependency-check
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - job: a11y
      optional: true
    - job: postman-dev02
      optional: true
    - job: commerce-dev01
      optional: true
    - job: copilot-dev02
      optional: true
    # Add stg02 jobs.
    - job: postman-stg02
      optional: true
    - job: commerce-stg02
      optional: true
    - job: copilot-stg02
      optional: true
    # Add prod02 jobs
    - job: commerce-prod02
      optional: true
    - job: copilot-prod02
      optional: true
    - job: postman-prod02
      optional: true
    - job: browser-performance
      optional: true

.get-clients-sandbox-demo-base:
  extends:
    - .get-clients
  stage: prepare-SANDBOX
  dependencies: []
  needs: []
  rules:
    - *skip-sandbox-commerce-stage
    - <<: *main-branch
      when: always

get-clients-sandbox:
  extends:
    - .get-clients-sandbox-demo-base
  variables:
    ENV_NAME: sandbox copilot
    STAGES: uat01 dev01 poc01
    PARENT_STAGE: $SANDBOX_COMMERCE_STAGE

get-clients-demo:
  stage: prepare-DEMO
  extends:
    - .get-clients-sandbox-demo-base
  variables:
    ENV_NAME: demo
    STAGES: demo01 demo02 demo03
    PARENT_STAGE: $SANDBOX_COMMERCE_STAGE

commerce-sandbox:
  stage: deploy-SANDBOX
  variables:
    PARENT_JOB_TOKEN: $CI_JOB_TOKEN
  trigger:
    include:
      - artifact: generated-config.yml
        job: get-clients-sandbox
    strategy: depend
  needs:
    - approve-sandbox-deploy
    - get-clients-sandbox
  rules:
    - *skip-sandbox-commerce-stage
    - *main-branch

commerce-sandbox-us-east-2:
  stage: deploy-SANDBOX
  extends:
    - .sls-deploy-10
  environment:
    name: $SANDBOX_ENV_NAME/drcommerce/${SANDBOX_COMMERCE_STAGE}-commerce
  variables:
    REGION: us-east-2
    PLATFORM: commerce
    CLIENT: drcommerce
    STAGE: uat01
  dependencies: []
  needs:
    - approve-sandbox-deploy
  rules:
    - *skip-sandbox-commerce-stage
    - *main-branch

commerce-demo:
  stage: deploy-DEMO
  variables:
    PARENT_JOB_TOKEN: $CI_JOB_TOKEN
  trigger:
    include:
      - artifact: generated-config.yml
        job: get-clients-demo
    strategy: depend
  needs:
    - approve-sandbox-deploy
    - get-clients-demo
  rules:
    - *skip-sandbox-commerce-stage
    - *main-branch

copilot-sandbox:
  stage: deploy-SANDBOX
  extends:
    - .sls-deploy-10
  environment:
    name: $SANDBOX_ENV_NAME/${SANDBOX_COPILOT_STAGE}-copilot
  variables:
    PLATFORM: copilot
    CLIENT: $SANDBOX_COPILOT_CLIENT
    STAGE: $SANDBOX_COPILOT_STAGE
  dependencies: []
  needs:
    - approve-sandbox-deploy
  rules:
    - *skip-sandbox-copilot-stage
    - *main-branch

copilot-beta:
  stage: deploy-SANDBOX
  extends:
    - .sls-deploy-10
  environment:
    name: $SANDBOX_ENV_NAME/beta-copilot
  variables:
    PLATFORM: copilot
    CLIENT: beta
    STAGE: $BETA_SANDBOX_COPILOT_STAGE
  needs:
    - approve-sandbox-deploy
  rules:
    - *skip-beta-copilot-stage
    - *skip-sandbox-copilot-stage
    - *main-branch

approve-production-deploy:
  stage: production-approve
  environment:
    name: production-approve
    action: prepare
  script:
    - echo $GITLAB_USER_NAME approves of this deployment.
  dependencies: []
  rules:
    - *skip-all-prod
    - *main-branch
  needs:
    # Copy stg02s needs
    - build
    - job: code-quality
      optional: true
    - job: dependency-check
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - job: a11y
      optional: true
    - job: postman-dev02
      optional: true
    - job: commerce-dev01
      optional: true
    - job: copilot-dev02
      optional: true
    # Add stg02 jobs.
    - job: postman-stg02
      optional: true
    - job: commerce-stg02
      optional: true
    - job: copilot-stg02
      optional: true
    # Add prod02 jobs
    - job: commerce-prod02
      optional: true
    - job: copilot-prod02
      optional: true
    - job: postman-prod02
      optional: true
    - job: browser-performance
      optional: true
    # Add sandbox jobs. These are required.
    - job: commerce-sandbox
      optional: true
    - job: copilot-sandbox
      optional: true
    - job: copilot-beta
      optional: true

get-clients-production:
  stage: prepare-PROD
  extends: .get-clients
  variables:
    ENV_NAME: live production
    STAGES: prod01
    PARENT_STAGE: $PROD_COMMERCE_STAGE
  rules:
    - *skip-prod-commerce-stage
    - <<: *main-branch
      when: always
  dependencies: []
  needs: []

commerce-production:
  stage: deploy-PROD
  trigger:
    include:
      - artifact: generated-config.yml
        job: get-clients-production
    strategy: depend
  variables:
    PARENT_JOB_TOKEN: $CI_JOB_TOKEN
  rules:
    - *skip-prod-commerce-stage
    - *main-branch
  allow_failure: true
  needs:
    - approve-production-deploy
    - get-clients-production

copilot-production:
  stage: deploy-PROD
  extends: .sls-deploy-10
  environment:
    name: $PROD_ENV_NAME/${PROD_COPILOT_STAGE}-copilot
  variables:
    PLATFORM: copilot
    CLIENT: $PROD_COPILOT_CLIENT
    STAGE: $PROD_COPILOT_STAGE
  dependencies: []
  rules:
    - *skip-prod-copilot-stage
    - *main-branch
  needs:
    - approve-production-deploy


commerce-client-deploy:
  stage: deploy-Selective
  extends: .sls-deploy-10
  variables:
    PLATFORM: $TARGET_PLATFORM
    STAGE: $TARGET_STAGE
    CLIENT: $TARGET_CLIENT
    BUILD_ID: $TARGET_COMMIT_ID 
    apiName: $TARGET_API_NAME
    functionSet: $REQ_FUNCTION_SET
  rules: 
    - *run-on-trigger
    - when: never