include:
  - project: fabric2-public/cicd/cicd-templates
    ref: main
    file:
      - templates/base_docker.yml
      - templates/storefront-react.yml
      - templates/code_quality.yml
      - templates/sast.yml
      - templates/secret_detection.yml
      - templates/dependency-check.yml

stages:
  - test
  - build
  - deploy-to-dev
  - sandbox-approve
  - build-sandbox
  - deploy-to-uat
  - production-approve
  - build-production
  - deploy-to-production

variables:
  BUILD_ID: $CI_COMMIT_SHORT_SHA
  STOREFRONT_NAME: "" # Defined in inheriting template
  IMAGE_AND_TAG: "${ECR_REGISTRY_DOMAIN}/${STOREFRONT_NAME}:${CI_ENVIRONMENT_SLUG}-${BUILD_ID}"
  PNI_DOCKERFILE_PREFIX: "" #Defined in .gitlab-ci.yml of branches - PNI_BP_Master, PNI_CA_Master, PNI_US_Master and PNI_BP_Help_Master

.rules-anchor:
  rules:
    - &main-branch
      if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH == "PHASE2-dev"'
      when: manual
    - &main-or-merge-request
      if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH == "PHASE2-dev" || $CI_MERGE_REQUEST_IID'
    - &skip-uat
      if: '$SKIP_UAT == "true"'
      when: never
    - &skip-sonarcloud
      if: '$SKIP_SONARCLOUD == "true"'
      when: never
    - &skip-codequality
      if: '$SKIP_CODE_QUALITY == "true"'
      when: never
    - &skip-semgrep
      if: '$SKIP_SEMGREP == "true"'
      when: never
    - &skip-secret-scanner
      if: '$SKIP_SEMGREP == "true"'
      when: never
    - &skip-dependency-check
      if: '$SKIP_DEPENDENCY_CHECK == "true"'
      when: never
    - &pni-implementation
      if: '$CI_COMMIT_BRANCH == "PNI_BP_Master" || $CI_COMMIT_BRANCH == "PNI_CA_Master" || $CI_COMMIT_BRANCH == "PNI_US_Master" || $CI_COMMIT_BRANCH == "PNI_BP_Help_Master"'
    - &skip-dev
      if: '$SKIP_DEV == "true"'
      when: never
    
    
.build:
  extends: .dind
  script:
    - | 
      apk add --no-cache \
        python3 \
        py3-pip \
        jq \
      && pip3 install --upgrade pip \
      && pip3 install \
        awscli \
      && rm -rf /var/cache/apk/*
    - aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $ECR_REGISTRY_DOMAIN
    - echo "$ARG_VALUE $SF_SECRETS_NAME_REF $CI_ENVIRONMENT_NAME"
    - |
      if [[ -z "$SF_SECRETS_NAME_REF" ]]
      then
        
        # Differentiation between PNI and other repos. 
        if [[ -z "$PNI_DOCKERFILE_PREFIX" ]]
        then
          echo "Regular Build"
          docker build -t "${IMAGE_AND_TAG}" --no-cache --build-arg NPM_RUN_CMD="${ARG_VALUE}" --build-arg ENV_NAME="${CI_ENVIRONMENT_NAME}" .
        else
          echo "PNI Build"
          IMAGE_AND_TAG="${ECR_REGISTRY_DOMAIN}/${STOREFRONT_NAME}:${CI_ENVIRONMENT_SLUG}-${PNI_DOCKERFILE_PREFIX}-${BUILD_ID}"
          docker build -t "${IMAGE_AND_TAG}" -f Dockerfile.${PNI_DOCKERFILE_PREFIX} .
        fi
      else
        echo "AWS Secrets Retreival Build"
        docker build -t "${IMAGE_AND_TAG}" --no-cache --build-arg SF_SECRETS_NAME_REF="${SF_SECRETS_NAME_REF}" --build-arg ENV_NAME="${CI_ENVIRONMENT_NAME}" --build-arg NPM_RUN_CMD="${ARG_VALUE}" . 
      fi
    - docker push "${IMAGE_AND_TAG}"
  variables:
    IMAGE: $IMAGE_AND_TAG

.base-test:
  stage: test
  dependencies: []

dependency-check:
  extends:
    - .base-test
    - .dependency-check
  rules:
    - *skip-dependency-check
    - *main-or-merge-request

code-quality:
  extends:
    - .code-quality
    - .base-test
  rules:
    - *skip-codequality
    - *main-or-merge-request

secret-scanner:
  extends:
    - .secret_detection_default_branch
    - .base-test
  rules:
    - *skip-secret-scanner
    - *main-or-merge-request

semgrep-sast:
  cache: {}
  extends:
    - .semgrep-sast
    - .base-test
  rules:
    - *skip-semgrep
    - *main-or-merge-request

sonarcloud:
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  extends:
    - .base-test
  allow_failure: true
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
  rules:
    - *skip-sonarcloud
    - *main-or-merge-request

build-dev:
  stage: build
  extends: .build
  environment:
     name: development
     action: prepare
  variables:
    ARG_VALUE: $ARG_VALUE
    SF_SECRETS_NAME_REF: $SF_SECRET_NAME
  rules:
     - *skip-dev
     - *main-or-merge-request

dev-deploy:
  stage: deploy-to-dev
  extends: .deploy_storefront
  environment:
    name: development
  rules:
    - *skip-dev
    - *main-branch
  needs: ["build-dev"]

sandbox-approve:
  stage: sandbox-approve
  environment:
    name: sandbox-approve
    action: prepare
  script:
    - echo $GITLAB_USER_NAME approves of this deployment.
  rules:
    - *skip-dev
    - *skip-uat
    - *pni-implementation
    - *main-branch
  needs: 
   - job: dev-deploy
     optional: true

build-uat:
  stage: build-sandbox
  extends: .build
  environment:
     name: uat
     action: prepare
  variables:
    ARG_VALUE: $ARG_VALUE
    SF_SECRETS_NAME_REF: $SF_SECRET_NAME
  rules:
     - *skip-uat
     - *pni-implementation
     - *main-branch
  needs: 
   - job: sandbox-approve
     optional: true
    

uat-deploy:
  stage: deploy-to-uat
  extends: .deploy_storefront
  environment:
    name: uat
  rules:
    - *skip-uat
    - *pni-implementation
    - *main-branch
  needs: ["build-uat"]

production-approve:
  stage: production-approve
  environment:
    name: production-approve
    action: prepare
  script:
    - echo $GITLAB_USER_NAME approves of this deployment.
  rules:
    - *pni-implementation
    - *main-branch
  needs:
  - job: uat-deploy
    optional: true

build-prod:
  stage: build-production
  extends: .build
  environment:
    name: production
    action: prepare
  variables:
    ARG_VALUE: $ARG_VALUE
    SF_SECRETS_NAME_REF: $SF_SECRET_NAME
  rules:
     - *pni-implementation
     - *main-branch
  needs: ["production-approve"]

prod-deploy:
  stage: deploy-to-production
  extends: .deploy_storefront
  environment:
    name: production
  rules:
    - *pni-implementation
    - *main-branch
  needs: ["build-prod"]

prod-deploy-east2:
  stage: deploy-to-production
  extends: .deploy_storefront
  environment:
    name: production
  variables:
    AWS_SELECTED_REGION: us-east-2
  rules:
    - *pni-implementation
    - *main-branch
  needs: ["build-prod"]
