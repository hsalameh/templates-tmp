local clientString = std.extVar('client_list');
local apiString = std.extVar('api_list');
local stage = std.extVar('stage');
local pipelineStage = std.extVar('pipeline_stage');
local client_list = std.split(clientString, " ");
local platform = std.extVar('platform');
local function_set = std.extVar('function_set');
local region = std.extVar('region');
local api_list = std.split(apiString, " ");

local variables = {

};

local workflow = {};

local stages = {
  stages: [pipelineStage]
};

local commerce_deploy_job(client, api, region) =
	local template_vars = { 
		region: region,
		api: api, 
		stage: stage, 
		client: client,
		platform: platform,
		function_set: function_set
	};
  {
		image: 'registry.gitlab.com/fabric2-public/cicd/cicd-templates/node-14-toolbox:v0.0.3',
    stage: pipelineStage,
		tags: ['cpu-optimized'],
		environment: {
			name: "%(api)s/%(stage)s/%(client)s" % template_vars,
		},
		retry: 2,
		script: |||
			#!/usr/bin/env bash		

			set -eux
			export AWS_REGION=%(region)s
			export FUNCTION_SET=%(function_set)s
			export API_NAME=%(api)s
			export STAGE=%(stage)s
			export CLIENT=%(client)s
			export PLATFORM=%(platform)s
			export SERVICE_NAME=%(platform)sApi-%(api)s

			function update_deploy_status() {
				echo "Build status: $1"
				cat <<EOF >post.json
				{
					"repo": "${CI_PROJECT_NAME}",
					"client": "${CLIENT}",
					"buildNumber": "${CI_JOB_ID}", 
					"buildStatus": "${1}"
				}
			EOF
				curl \
					--silent \
					--location \
					--request POST \
					--header "x-api-key:${PLATFORM_API_KEY}" \
					--header "Content-Type:application/json" \
					--data-binary @post.json "${PLATFORM_BASE_URL}"/updateBuild
			}

			functionSet=${FUNCTION_SET:-"empty"}

			if [[ "$CI_PROJECT_NAME" == "fabric-service-external" ]]; then 

				declare -A function_array

				function_array['payment']="authorize-net"
				function_array['shipment']="shippo"
				function_array['shippment']="shippo"
				function_array['subscription']="subscription"
				function_array['address']="avalara"
				function_array['tax']="avalara"
				function_array['klaviyo']="klaviyo"
				function_array['taxjar']="taxjar"
				function_array['shipstation']="shipstation"

				export FUNCTION_SET="${function_array[$API_NAME]}"
				export SERVICE_NAME=external-%(api)s
				echo "Function Set Name:= ${FUNCTION_SET}"
				functionSet="${FUNCTION_SET}"
			fi

			cat <<EOF >post.json
			{
				"repo": "${CI_PROJECT_NAME}",
				"client": "${CLIENT}",
				"build": {
					"buildName": "${CLIENT}-${STAGE}-${API_NAME}",
					"commitTime": "${CI_COMMIT_TIMESTAMP}",
					"buildNumber": "${CI_JOB_ID}",
					"pipelineId": "${CI_PIPELINE_ID}",
					"deployedBy": "${GITLAB_USER_EMAIL}",
					"commitId": "${CI_COMMIT_SHORT_SHA}",
					"functionSet": "${functionSet}",
					"platform": "${PLATFORM}",
					"apiName": "${API_NAME}",
					"stage": "${STAGE}",
					"jobUrl": "${CI_JOB_URL}",
					"buildStatus": "RUNNING",
					"createdAt": $(date +%%s\%%N | cut -b1-13)
				}
			}
			EOF
			cat post.json
			curl \
				--silent \
				--location \
				--request POST \
				--header "x-api-key:${PLATFORM_API_KEY}" \
				--header "Content-Type:application/json" \
				--data-binary @post.json "${PLATFORM_BASE_URL}/addToHistory"

			echo //registry.npmjs.org/:_authToken=${NPM_TOKEN} > ~/.npmrc  && npm install
			git clone -b ${DEPLOY_CONFIG_BRANCH} ${DEPLOY_CONFIG_REPO}
			cd deployment-scripts
			npm install

			status=FAILED
			exitcode=1

			if node deploy.js; then
				status=SUCCESS
				exitcode=0
			fi

			update_deploy_status $status
			exit $exitcode
		||| % template_vars,
    when: 'always',
    needs: []
  };

local jobs = {
	[client+"-"+stage+"-"+api]: commerce_deploy_job(client, api, region)  
	for client in client_list 
	for api in api_list 
};

std.manifestYamlDoc(workflow + stages + jobs)

