// jsonnet -S --ext-str stage_suffix="" --ext-str pipeline_env="SANDBOX" --client_list="clientA:dev01 clientB:uat01" commerce_deploy_json.jsonnet
local clientString = std.extVar('client_list');
local stageSuffix = std.extVar('stage_suffix');
local pipelineEnv = std.extVar('pipeline_env');
local clientList = std.split(clientString, " ");
local pipelineStage = "deploy-" + pipelineEnv;

local include = {
  include: [{
    project: "fabric2-public/cicd/cicd-templates",
    ref: "main",
    file: ["templates/sls.yml"]
  }]
};

local workflow = {
  workflow: {
    rules: [
      {
        "if": "$CI_COMMIT_BRANCH == \"drcommerce\""
      },
      {
        "if": "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      },
    ],
  },
};

local stages = {
  stages: [
    'echo',
    pipelineStage
  ]
};

local commerce_deploy_job(clientStage) =
  local clientAndStage = std.split(clientStage, ":");
  local client = clientAndStage[0];
  local stage = clientAndStage[1];
  {

    stage: pipelineStage,
    extends: '.sls-deploy-14',
    environment: {
      name: pipelineEnv + '/' + client + '/' + stage + stageSuffix
    },
    variables: {
      STAGE: stage + stageSuffix,
      CLIENT: client,
      PLATFORM: 'commerce',
    },
    when: 'manual',
    needs: []
  };

local jobs = {[clientStage]: 
  commerce_deploy_job(clientStage) for clientStage in clientList };

local echoJob = {
  echoJob: {
    stage: 'echo',
    image: 'alpine',
    script: [
      'echo "This job is needed to keep the pipeline from failing. Child pipelines need at least one non-manual job."',
      'env'
    ],
    needs: []
  },
};

local gitlabCiConf = include + workflow + stages + jobs + echoJob;

std.manifestYamlDoc(gitlabCiConf)