# Serverless Pipeline

- [Serverless Pipeline](#serverless-pipeline)
  - [Setup](#setup)
    - [Create repository](#create-repository)
    - [Mirror repository with Bitbucket](#mirror-repository-with-bitbucket)
    - [Set default branch to main](#set-default-branch-to-main)
    - [Set up CICD Variables](#set-up-cicd-variables)
    - [Add .gitlab-ci.yml](#add-gitlab-ciyml)
    - [Update serverless.yml](#update-serverlessyml)
    - [Update serverless-webpack plugin to ^5.4.0](#update-serverless-webpack-plugin-to-540)
  - [Protecting sandbox and production deployments](#protecting-sandbox-and-production-deployments)
  - [Stages](#stages)
    - [Validate](#validate)
    - [Build](#build)
      - [Build artifacts and local testing](#build-artifacts-and-local-testing)
    - [Test](#test)
    - [Deploy Dev (dev02)](#deploy-dev-dev02)
    - [Test Dev](#test-dev)
    - [Deploy QA (stg02)](#deploy-qa-stg02)
    - [Test QA](#test-qa)
    - [Deploy Internal UAT (prod02)](#deploy-internal-uat-prod02)
    - [Test Internal UAT](#test-internal-uat)
    - [Sandbox Approval](#sandbox-approval)
    - [Deploy Sandbox](#deploy-sandbox)
    - [Prod Approval](#prod-approval)
    - [Deploy Prod](#deploy-prod)
  - [Integration with Platform API](#integration-with-platform-api)
  - [Overriding Client & Stage Names](#overriding-client--stage-names)
  - [Deploy switches](#deploy-switches)
  - [Hotfixes & Skipping Jobs](#hotfixes--skipping-jobs)
  - [Artifact Promotion and Rollbacks](#artifact-promotion-and-rollbacks)
    - [Promotions](#promotions)
    - [Rollbacks](#rollbacks)
  - [Reporting](#reporting)
    - [Merge request widgets](#merge-request-widgets)
    - [Pipeline reports](#pipeline-reports)
  - [Pipeline Variables Reference](#pipeline-variables-reference)
  - [Gitlab Rollout Plan](#gitlab-rollout-plan)
    - [Deploy](#deploy)
      - [Add new path mapping](#add-new-path-mapping)
      - [Update sandbox and prod stage names](#update-sandbox-and-prod-stage-names)
      - [Run the pipeline](#run-the-pipeline)
      - [Setup API key](#setup-api-key)
    - [Validate Release](#validate-release)
    - [Cutover](#cutover)

The [serverless.yml](https://gitlab.com/fabric2-public/cicd/cicd-templates/-/blob/main/templates/pipelines/serverless.yml) pipeline is a complete, end-to-end pipeline for serverless projects. It will automatically build, test, and deploy code to dev02 when code is checked in. Thereafter each deployment requires a button click. The pipeline includes the following stages labelled with the context in which they run:

- validate
- build
- test
- deploy-DEV
- test-DEV
- deploy-QA
- test-QA
- deploy-internal-UAT
- test-internal-UAT
- deploy-SANDBOX
- deploy-DEMO (optional)
- deploy-PROD

## Setup

### Create repository

On the Gitlab fabric2 dashboard, click the **New Project** button and select **Create Blank Project**. Enter the name of the migrating project and create it.

### Mirror repository with Bitbucket

Navigate to **Settings → Repository → Mirroring Repositories** and enter the URL of the migrating repository (e.g. `https://<username>@bitbucket.org/ydv-all/fabric-service-inventory.git`). Note the direction is **Pull**. Enter your Bitbucket password and save. Repeat this step for **Push**.

### Set default branch to main

Navigate to **Settings → Repository → Default branch** and set it to **main**. If main doesn’t exist, branch it off of the current default branch and push it to Gitlab.

Find the **Protected branches** section, add select main from the branch dropdown. In the **Allowed to merge** dropdown select **Developers + Maintainers**. In the **Allowed to push** dropdown select **No One**. Click the Protect button.

### Set up CICD Variables

Go to the project's **Settings → CICD → Variables** page and add the following required variables:

_These has already been set at organization level so doesn’t need to be set by everyone._

|Name|Desc|
|--- |--- |
|AWS_ACCESS_KEY_ID|AWS Access key. The key needs sts:AssumeRole permissions to the OrganizationAccountAccessRole, so you may need to speak with DevOps to set this up.|
|AWS_SECRET_ACCESS_KEY|Corresponding AWS secret|

### Add .gitlab-ci.yml

Create a `.gitlab-ci.yml` file in the root directory of your project and add the following yaml:

```yaml
include:
  - project: fabric2-public/cicd/cicd-templates
    ref: main
    file:
      - templates/pipelines/serverless.yml

variables:      
  NPM_RUN_UNIT_TEST_CMD: test:coverage # your `npm run` test command
  NPM_RUN_LINT_CMD: lint # your `npm run` lint command
```

This will get the **bare-minimum** pipeline set up. See the _Variables_ section below for a full list of variables and behaviors associated with them, which will enable other jobs in the pipeline.

### Update serverless.yml

Change `GIT_REVISION: ${env:GIT_REVISION}` to `GIT_REVISION: ${env:CI_COMMIT_SHORT_SHA}`.

Some variables will not be set in the build stage that will cause builds to fail. A common culprit is `${opt:client}`. In that case set a default, such as `${opt:client,"build"}`, as these variables are set later in the deploy stage.

### Update serverless-webpack plugin to ^5.4.0

Using existing webpack zip files for deployments was introduced in serverless-webpack plugin 5.4.0.

```bash
npm install --save-dev serverless-webpack@^5.4.0
```

## Protecting sandbox and production deployments

Both sandbox and production deployments can be restricted by special approval environments: _sandbox-approval_ and _prod-approval_. In the _Protected Environments_ section in the CICD settings, select these approval environments and specify users that are authorized to deploy to these environments. Only these authorized users will have the option to run the approval jobs.

## Stages

### Validate

Runs lint and unit-test checks.

|Job|Description|Variables|
|--- |--- |--- |
|lint|Runs an npm lint command defined by `NPM_RUN_LINT_CMD` variable.|`NPM_RUN_UNIT_LINT_CMD` `BUILD_DIR`|
|unit-test|Runs an npm unit test command defined by `NPM_RUN_UNIT_TEST_CMD` variable.|`NPM_RUN_UNIT_TEST_CMD` `NPM_JUNIT_REPORT_PATH` `NPM_COBERTURA_REPORT_PATH` `BUILD_DIR`|

### Build

Builds are decoupled from deployments, so pipelines require just a single build to deploy to any number of environments. Builds are stored in the project's package registry, first under the \`build\` tag and subsequently promoted to higher environments as the pipeline progresses.

Under the hood, build runs the `webpack.sh` script: [https://gitlab.com/fabric2-public/cicd/cicd-templates/-/blob/main/src/scripts/sls/webpack.sh](https://gitlab.com/fabric2-public/cicd/cicd-templates/-/blob/main/src/scripts/sls/webpack.sh) .

#### Build artifacts and local testing

Build artifacts contain all of the files to run and test the service. Go to the repo's package repository at `https://gitlab.com/<repo-slug>/-/packages` for a list of build artifacts. You can even download, unzip, and run artifacts locally.

|Job|Description|Variables|
|--- |--- |--- |
|build|Builds the serverless artifact.|BUILD_DIR|

### Test

Test runs a suite of quality gate reports, all of which can be viewed via merge request widgets or through the project's reporting dashboard.

|Job|Description|
|--- |--- |
|dependency-check|Runs [OWASP dependency-check](https://owasp.org/www-project-dependency-check/).|
|sonarcloud|Runs sonarcloud scan on codebase.|
|code-quality|Runs codeclimate(https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html ) code quality report on codebase.Code Quality requires a .codeclimate.yml configuration file.|
|secret-scanner|Runs gitleaks (https://github.com/zricethezav/gitleaks ) secret detection report.|
|semgrep-sast|Runs semgrep (https://semgrep.dev/ ) static analysis security report on codebase.|

### Deploy Dev (dev02)

|Job|Description|
|--- |--- |
|copilot-dev02|Deploys serverless build artifact to dev02 greatwall-internal.|
|commerce-dev01|Deploys serverless build artifact to dev01 bailey.|

The deploy-DEV stage deploys the build artifact to the dev02 copilot account on greatwall-internal and the dev01 commerce account on bailey. This (and the following) deployments are all recorded in [https://platform.fabric.zone](https://platform.fabric.zone) .

### Test Dev

|Job|Description|
|--- |--- |
|postman-dev02|Runs optional Postman tests against the dev environment. The Postman test can be enabled by added the POSTMAN_DEV02_ENVIRONMENT_UUID environment variable to the pipeline. See Variables section below for more info on where to find the UUID.|
|a11y|Runs a11y accessibility report https://www.a11yproject.com/ on the url defined by A11Y_URL variable.|

### Deploy QA (stg02)

|Job|Description|
|--- |--- |
|copilot-stg02|Deploys serverless build artifact to stg02 copilot greatwall-internal|
|commerce-stg02|Deploys serverless build artifact to stg02 commerce bailey|

### Test QA

|Jobs|Description|
|--- |--- |
|postman-stg02|Runs optional Postman tests against the stage environment. The Postman test can be enabled by added the POSTMAN_STG02_ENVIRONMENT_UUID environment variable to the pipeline.|

Runs optional Postman tests if `POSTMAN_STG02_ENVIRONMENT_UUID` is defined.

### Deploy Internal UAT (prod02)

|Jobs|Description|
|--- |--- |
|copilot-prod02|Deploys serverless build artifact to prod02 copilot greatwall-production|
|commerce-prod02|Deploys serverless build artifact to prod02 commerce|

Deploys the build artifact to commerce/bailey and copilot/greatwall-production on prod02.

### Test Internal UAT

|Jobs|Description|
|--- |--- |
|postman-prod02|Runs optional Postman tests if POSTMAN_PROD02_ENVIRONMENT_UUID is defined.|
|browser-performance|Runs optional browser performance test via sitespeed (https://www.sitespeed.io/ ) browser performance report on the url provided by the PERF_TEST_URL variable.|

### Sandbox Approval

|Jobs|Description|
|--- |--- |
|approve-sandbox-deploy|Introduces a "sandbox-approval" environment can be controlled in *Settings -> CICD -> Protected Environments.|

### Deploy Sandbox

|Jobs|Description|
|--- |--- |
|get-clients-sandbox|Calls the Platform API to fetch all clients in the sandbox environment.|
|copilot-sandbox|Deploys serverless build artifact to sandbox copilot greatwall-sandbox|
|commerce-sandbox|Deploys serverless build artifact to sandbox commerce environments fetched from get-clients-sandbox.|

Deploy sandbox uses the [https://platform.fabric.zone](https://platform.fabric.zone) API to fetch all active commerce environments in sandbox. It then generates a dynamic child pipeline to deploy to each commerce account in parallel. The deploy jobs are all manually triggered.

### Prod Approval

|Jobs|Description|
|--- |--- |
|approve-production-deploy|Introduces a "prod-approval" environment can be controlled in *Settings -> CICD -> Protected Environments.|

### Deploy Prod

|Jobs|Description|
|--- |--- |
|get-clients-production|Calls the Platform API to fetch all clients in the production environment.|
|copilot-production|Deploys serverless build artifact to production copilot greatwall-production|
|commerce-production|Deploys serverless build artifact to production commerce environments fetched from get-clients-production.|

Deploy prod uses the [https://platform.fabric.zone](https://platform.fabric.zone) API to fetch all active commerce environments in production. It then generates a dynamic child pipeline to deploy to each commerce account in parallel. The deploy jobs are all manually triggered.

## Integration with Platform API

The pipeline uses the Platform API to fetch a list of active commerce accounts to deploy for sandbox and production environments. Each of these jobs must be manually triggered.

![](attachments/2081849644.png)

## Overriding Client & Stage Names

Set the client environment variables to a custom name if you wish to change the target AWS account to deploy to. If a service's dev environment is not greatwall-internal, then set the `DEV_COPILOT_CLIENT` or `DEV_COMMERCE_CLIENT` variable to the name of the target account as it appears in the client->account ID secret.

|Client Variables|
|--- |
|`DEV_COPILOT_CLIENT`|
|`DEV_COMMERCE_CLIENT`|
|`QA_COPILOT_CLIENT`|
|`QA_COMMERCE_CLIENT`|
|`INT_UAT_COPILOT_CLIENT`|
|`INT_UAT_COMMERCE_CLIENT`|
|`SANDBOX_COPILOT_CLIENT`|
|`SANDBOX_COMMERCE_CLIENT`|
|`PROD_COPILOT_CLIENT`|
|`PROD_COMMERCE_CLIENT`|

Similarly, override the stage name variable (see table below) with a custom value to change the stage name. 

## Deploy switches

These environment variables control certain aspects of the serverless pipeline.

|Name|Description
|--- |--- |
|`SKIP_SECRETS`| Set to a truthy value to skip fetching secrets from secretmanager and parameter store.|

## Hotfixes & Skipping Jobs

These variables can skip code and security gates in the pipeline if their value is set to anything _truthy_.

|Name|
|--- |
|`SKIP_LINT`|
|`SKIP_UNIT_TEST`|
|`SKIP_SONARCLOUD`|
|`SKIP_CODE_QUALITY`|
|`SKIP_SEMGREP`|
|`SKIP_SECRET_SCANNER`|

These variables control environment deployments. If they are set to an empty value (e.g. `DEV_COPILOT_STAGE: ''`, the deployment will be skipped.

|Name|
|--- |
|`DEV_COPILOT_STAGE`|
|`DEV_COMMERCE_STAGE`|
|`QA_COPILOT_STAGE`|
|`QA_COMMERCE_STAGE`|
|`INT_UAT_COPILOT_STAGE`|
|`INT_UAT_COMMERCE_STAGE`|
|`SANDBOX_COPILOT_STAGE`|
|`SANDBOX_COMMERCE_STAGE`|
|`PROD_COPILOT_STAGE`|
|`PROD_COMMERCE_STAGE`|

For example, if you are deploying a service that only runs in commerce, your variables block would contain the following:

```yaml
variables:
  DEV_COPILOT_STAGE: ''
  QA_COPILOT_STAGE: ''
  INT_UAT_COPILOT_STAGE: ''
  SANDBOX_COPILOT_STAGE: ''
  DEMO_COPILOT_STAGE: ''
  PROD_COPILOT_STAGE: ''
```

## Artifact Promotion and Rollbacks

The template offers a number of advantages over the old Bitbucket way of doing things.

Rather than rebuilding for each environment, this template is set up to build once, deploy anywhere. The build step creates a stage-agnostic build artifact (a collection of zip files) and stores them in the project’s package registry `GITLAB_PROJECT_URL/-/packages`.

### Promotions

When a deploy job is triggered, a few things happen:

1. The pipeline promotes the build artifact to the target environment’s registry folder
2. Serverless generates the Cloudformation templates for the target environment
3. Serverless deploys the artifacts and the Cloudformation templates to AWS
    
### Rollbacks

Rollbacks use the existing build artifact to redeploy a previous build. A rollback will only run the deployment job in a pipeline and not the tests or quality gates.

To perform a rollback, navigate to the environments page. From the left-hand nav hover over **Deployments** and select **Environments**. Select the target environment.

For example, to rollback a commerce deployment in dev01, expand the `DEV` environment and select dev01-commerce.

![](attachments/2136146277.png)

You will be presented with a list of deployments to the target environment from which you can choose a specific build to roll back to. To trigger the rollback, click the counter-clockwise arrow next to the build.

![](attachments/2134377768.png)

The deployment step to that environment will re-run with the existing build artifact.

## Reporting

After a pipeline runs, gitlab offers several ways to view the results of the quality gates and security scan reporting.

### Merge request widgets

If the pipeline run was triggered by a merge request, the merge request will embed report widgets.

![](attachments/2082046154.png)

### Pipeline reports

You can view code quality changes, security scan results, and unit test reporting in full on the pipeline page. Click the pipeline and select the tests, security, and code quality tabs. These reports can also be accessed by clicking the `View full report` button in the merge request widget.

![SAST report](attachments/2082111652.png)![](attachments/2081587416.png)

## Pipeline Variables Reference

See [https://gitlab.com/fabric2-public/cicd/cicd-templates/-/blob/main/examples/serverless/.gitlab-ci.sls.yml](https://gitlab.com/fabric2-public/cicd/cicd-templates/-/blob/main/examples/serverless/.gitlab-ci.sls.yml) for an example configuration with reference.

|Name|Desc|Default|Required|
|--- |--- |--- |--- |
|`AWS_ACCESS_KEY_ID`|AWS access key||Yes|
|`AWS_SECRET_ACCESS_KEY`|AWS secret||Yes|
|`BUILD_DIR`|Relative path to build directory for lint, unit test, and build jobs.|./|No|
|`NPM_RUN_UNIT_TEST_CMD`|The npm run command to use for unit testing (e.g. test:coverage)||Yes|
|`NPM_RUN_LINT_CMD`|The npm run command to use for linting (e.g. lint)||Yes|
|`NPM_JUNIT_REPORT_PATH`|Path to the junit test report.|./junit.xml|No|
|`NPM_COBERTURA_REPORT_PATH`|Path to the cobertura code coverage report.|./cobertura-coverage.xml|No|
|`POSTMAN_COLLECTION_UUID`|Collection UUID. Get it here. **||No|
|`POSTMAN_DEV02_ENVIRONMENT_UUID`|Postman environment for dev02.*How to find it.||No|
|`POSTMAN_STG02_ENVIRONMENT_UUID`|Postman environment for stg02.*How to find it.||No|
|`POSTMAN_PROD02_ENVIRONMENT_UUID`|Postman environment for prod02.*How to find it.||No|
|`BROWSER_PERF_URL`|The URL to use for browser performance testing.*||No|
|`A11Y_URL`|The URL to use for a11y accessibility testing. *||No|

**\*** _If omitted, the pipeline will function but the job will be skipped._  
**\*\*** _If omitted, the pipeline will function but all Postman jobs will be skipped._

## Gitlab Rollout Plan

The rollout plan assumes that the application has been validated on dev02, and stg02/prod02 environments.

### Deploy

#### Add new path mapping

If the custom domain plugin is in the project, you can simply update the `custom.customDomain.basePath` variable to add a new path mapping to the domain. If the plugin is not installed, you may need to go into the APIG UI to manually add a new path mapping.

#### Update sandbox and prod stage names

Deploy copilot and commerce stack with a new stage name: `{stage}release`. In the .gitlab-ci.yml file, add the following to override the stage names:  

```yaml
  SANDBOX_COPILOT_STAGE: sandboxrelease
  SANDBOX_COMMERCE_STAGE: sandboxrelease
  PROD_COPILOT_STAGE: prod01release
  PROD_COMMERCE_STAGE: prod01release
```

1. This will result in a second API gateway. For example, in greatwall-production, XM will have:
    1. prod01release-copilot-xm (Gitlab-deployed stack)
    2. prod01-copilot-xm (Bitbucket-deployed stack)  
2. **Notes**
    1. Do not overwrite the Bitbucket deployed stack on prod accounts
    2. Teams will be responsible for making sure their entire stack is deployed with the different stage name.

#### Run the pipeline

Push these changes. When the pipeline reaches the sandbox deploy job, manually click the play button to trigger the deployment. Don’t click play on the production deploy job until the sandbox deployment has been validated.

Open the commerce deploy job and choose abchomenonprod-uat01, as this client and environment has the \`uat01release\` secrets and parameter store; choosing a client without the secrets set up will result in a failed deployment

#### Setup API key

Set the new, Gitlab-deployed API Gateway to use the API key from the old, Bitbucket-deployed API Gateway. This step is necessary to get the new API Gateway working with the existing service discovery.

1. Go to the API Gateway dashboard and click any API
2. Click “Usage Plans”  
![](attachments/2140406298.png)
3. Find and click the Gitlab-deployed API name (⌘+F is your friend)  
![](attachments/2139751137.png)
4. Click the **API Keys** tab and click the **Add API Key to Usage Plan** button
![](attachments/2139751165.png)
5. Type in the name of the original API Gateway and click the checkbox on the right.  
![](attachments/2130712788.png)
6. Now your new, Gitlab-deployed API will accept the old API’s key! :tada:

### Validate Release

1. Run manual/automated testing against the new deployment.

### Cutover

1. Update the API Gateway mapping so that the sandbox and production traffic is directed to the gitlab stack
2. ![](attachments/2127364422.png)
    1. Monitor Cloudwatch and Datadog
    2. Regression testing
3. If there is an issue, re-route the traffic to Bitbucket stack by resetting the API Gateway mapping to its old value.
4. Once everything is working, decommission Bitbucket (The timeline for this will be determined by the teams themselves)
