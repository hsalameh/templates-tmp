# Serverless NextJS Pipeline

- [Serverless NextJS Pipeline](#serverless-nextjs-pipeline)
  - [Setup](#setup)
    - [Prerequisites](#prerequisites)
    - [Create repository](#create-repository)
    - [Mirror repository with Bitbucket](#mirror-repository-with-bitbucket)
    - [Set default branch to main](#set-default-branch-to-main)
    - [Set up CICD Variables](#set-up-cicd-variables)
    - [Add .gitlab-ci.yml](#add-gitlab-ciyml)
  - [Stages](#stages)
    - [Dependencies](#dependencies)
    - [Validate](#validate)
    - [Build](#build)
    - [Test](#test)
    - [Deploy Dev](#deploy-dev)
    - [Test Dev](#test-dev)
    - [Deploy QA](#deploy-qa)
    - [Test QA](#test-qa)
    - [Deploy Sandbox](#deploy-sandbox)
    - [Deploy Prod](#deploy-prod)
  - [Hotfixes & Skipping Jobs](#hotfixes--skipping-jobs)
  - [Pipeline Variables Reference](#pipeline-variables-reference)

The nextjs pipeline template is a complete, end-to-end pipeline for static s3 website projects. The pipeline has two triggers. The pipeline includes the following stages labelled with the context in which they run:

- dependencies
- validate
- build
- test
- deploy-dev
- test-dev
- deploy-qa
- test-qa
- deploy-sandbox
- deploy-prod

Internal UAT (prod02) will be decommissioned in the near future and so will not be included in the pipeline.

## Setup

### Prerequisites

- S3 buckets have been configured for each environment

### Create repository

On the Gitlab fabric2 dashboard, click the **New Project** button and select **Create Blank Project**. Enter the name of the migrating project and create it.

### Mirror repository with Bitbucket

Navigate to **Settings → Repository → Mirroring Repositories** and enter the URL of the migrating repository (e.g. `https://<username>@bitbucket.org/ydv-all/fabric-service-inventory.git`). Note the direction is **Pull**. Enter your Bitbucket password and save. Repeat this step for **Push**.

### Set default branch to main

Navigate to **Settings → Repository → Default branch** and set it to **main**. If main doesn’t exist, branch it off of the current default branch and push it to Gitlab.

Find the **Protected branches** section, add select main from the branch dropdown. In the **Allowed to merge** dropdown select **Developers + Maintainers**. In the **Allowed to push** dropdown select **No One**. Click the Protect button.

### Set up CICD Variables

Go to the project's **Settings → CICD → Variables** page and add the following required variables:

_These has already been set at organization level so doesn’t need to be set by everyone._

|Name|Desc|
|--- |--- |
|`AWS_ACCESS_KEY_ID`|AWS Access key. The key needs sts:AssumeRole permissions to the OrganizationAccountAccessRole, so you may need to speak with DevOps to set this up.|
|`AWS_SECRET_ACCESS_KEY`|Corresponding AWS secret|

### Add .gitlab-ci.yml

Create a `.gitlab-ci.yml` file in the root directory of your project and add the following yaml replacing the variable values with the project’s:

```yaml
include:
  - project: fabric2-public/cicd/cicd-templates
    ref: main
    file:
      - templates/pipelines/nextjs.yml

variables:
  NPM_RUN_UNIT_TEST_CMD: test
  NPM_RUN_LINT_CMD: lint
  BUILD_CMD: npm run build
  DEV_S3_BUCKET: your-bucket-for-demo-env
  PROD_S3_BUCKET: your-bucket-for-prod-env
  DEV_ENV_URL: https://demo-site.fabric.inc
  PROD_ENV_URL: https://prod-site.fabric.inc
  DIST_DIR: "./dist"
```

This will get the **bare-minimum** pipeline set up. See the _Variables_ section below for details and a full list of variables.

## Stages

### Dependencies

|Job|Description|Variables|
|--- |--- |--- |
|install-deps|Runs npm install.|`BUILD_DIR`|

### Validate

Runs lint and unit-test checks.

|Job|Description|Variables|
|--- |--- |--- |
|lint|Runs an npm lint command defined by `NPM_RUN_LINT_CMD` variable.|`NPM_RUN_UNIT_LINT_CMD` `BUILD_DIR`|
|unit-test|Runs an npm unit test command defined by `NPM_RUN_UNIT_TEST_CMD` variable.| `NPM_RUN_UNIT_TEST_CMD` `NPM_JUNIT_REPORT_PATH` `NPM_COBERTURA_REPORT_PATH` `BUILD_DIR`|

### Build

Builds are decoupled from deployments, so pipelines require just a single build to deploy to any number of environments. Builds are stored in the project's package registry, first under the \`build\` tag and subsequently promoted to higher environments as the pipeline progresses.

Under the hood, build runs the `webpack.sh` script: [https://gitlab.com/fabric2-public/cicd/cicd-templates/-/blob/main/src/scripts/sls/webpack.sh](https://gitlab.com/fabric2-public/cicd/cicd-templates/-/blob/main/src/scripts/sls/webpack.sh) .

|Job|Description|Variables|
|--- |--- |--- |
|build|Builds the artifact|`BUILD_DIR` `BUILD_CMD`|

### Test

Test runs a suite of quality gate reports, all of which can be viewed via merge request widgets or through the project's reporting dashboard.

|Job|Description|
|--- |--- |
|dependency-check|Runs [OWASP dependency-check](https://owasp.org/www-project-dependency-check/).|
|sonarcloud|Runs sonarcloud scan on codebase.|
|code-quality|Runs [codeclimate](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) code quality report on codebase.Code Quality requires a .codeclimate.yml configuration file.|
|secret-scanner|Runs [gitleaks](https://github.com/zricethezav/gitleaks) secret detection report.|
|semgrep-sast|Runs [semgrep](https://semgrep.dev/ ) static analysis security report on codebase.|

### Deploy Dev

|Job|Description|
|--- |--- |
|dev:deploy|Deploys build artifact to dev s3 bucket specified by `DEV_S3_BUCKET` variable.|

### Test Dev

|Job|Description|
|--- |--- |
|dev:liveness-probe|Runs a simple curl request against the dev deployment URL specified by `DEV_ENV_URL` |
|dev:a11y (OPTIONAL)|Runs a11y accessibility testing against DEV_A11Y_URL if it is set. Disabled by default.|
|dev:browser-performance (OPTIONAL)|Runs browser performance testing against `DEV_BROWSER_PERF_URL` if it is set. Disabled by default.|

### Deploy QA

|Job|Description|
|--- |--- |
|qa:deploy|Deploys build artifact to QA s3 bucket specified by `QA_S3_BUCKET` variable.|

### Test QA

|Job|Description|
|--- |--- |
|qa:liveness-probe|Runs a simple curl request against the QA deployment URL specified by `QA_ENV_URL`|
|qa:a11y (OPTIONAL)|Runs a11y accessibility testing against `QA_A11Y_URL` if it is set. Disabled by default.|
|qa:browser-performance (OPTIONAL)|Runs browser performance testing against `QA_BROWSER_PERF_URL` if it is set. Disabled by default.|

### Deploy Sandbox

|Job|Description|
|--- |--- |
|sandbox:deploy|Deploys build artifact to the sandbox s3 bucket specified by `SANDBOX_S3_BUCKET` variable.|

### Deploy Prod

|Job|Description|
|--- |--- |
|prod:deploy|Deploys build artifact to QA s3 bucket specified by `PROD_S3_BUCKET` variable.|

## Hotfixes & Skipping Jobs

These variables can skip code and security gates in the pipeline if their value is set to anything _truthy_.

|Name|
|--- |
|`SKIP_LINT`|
|`SKIP_UNIT_TEST`|
|`SKIP_SONARCLOUD`|
|`SKIP_CODE_QUALITY`|
|`SKIP_SEMGREP`|
|`SKIP_SECRET_SCANNER`|

These variables control environment deployments. If any of these are set to an empty value the respective environment deployment will be skipped. For example, `QA_ENV: ''` will skip the QA deployment.

|Name|
|--- |
|`DEV_ENV`|
|`QA_ENV`|
|`SANDBOX_ENV`|
|`PROD_ENV`|

## Pipeline Variables Reference

See [https://gitlab.com/fabric2-public/cicd/cicd-templates/-/blob/main/examples/serverless/.gitlab-ci.sls.yml](https://gitlab.com/fabric2-public/cicd/cicd-templates/-/blob/main/examples/serverless/.gitlab-ci.sls.yml) for an example configuration with reference.

|Name|Desc|Default|Required|
|--- |--- |--- |--- |
|`AWS_ACCESS_KEY_ID`|AWS access key||Yes|
|`AWS_SECRET_ACCESS_KEY`|AWS secret||Yes|
|`BUILD_DIR`|Relative path to build directory for lint, unit test, and build jobs.|./|No|
|`DIST_DIR`|The directory that contains the build output||Yes|
|`BUILD_CMD`|Command to run to build the project. This can be anything; it’s not specific to npm. If you want to run an npm command, you need to set `BUILD_CMD` to the entire command: e.g. npm run build.||Yes|
|`NPM_RUN_UNIT_TEST_CMD`|The npm run command to use for unit testing (e.g. test:coverage)||Yes|
|`NPM_RUN_LINT_CMD`|The npm run command to use for linting (e.g. lint)||Yes|
|`NPM_COBERTURA_REPORT_PATH`|Path to the cobertura code coverage report.|./cobertura-coverage.xml|No|
|`DEV_S3_BUCKET` `QA_S3_BUCKET` `PROD_S3_BUCKET`|The name of the s3 bucket that hosts the service’s static contents (without the s3:// prefix)||Yes|
|`DEV_ENV_URL` `QA_ENV_URL` `PROD_ENV_URL`|The URL of the service.||Yes|
|`DEV_BROWSER_PERF_URL` `QA_BROWSER_PERF_URL` `PROD_BROWSER_PERF_URL`|The URL to use for browser performance testing.*||No|
|`DEV_A11Y_URL` `QA_A11Y_URL` `PROD_A11Y_URL`|The URL to use for a11y accessibility testing. *||No|
