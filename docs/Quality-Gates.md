## Quality Gate Templates

Unit testing, API testing, quality gates, etc.

### .jmeter

Runs [jmeter](https://jmeter.apache.org/) performance test.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| JMX_PATH | The file path to the jmx file | yes | |

### .a11y

Runs [a11y](https://www.a11yproject.com/) accessibility report on a URL.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| a11y_url | The URL to test | yes | |

### .browser-performance

Runs a [sitespeed](https://www.sitespeed.io/) browser performance report on a given URL.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| URL | The URL to test | yes | |

### .code-quality

Runs [codeclimate](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) code quality report.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |

### .ping

Runs a curl command to test for a http status code. The job fails if the status code is unexpected.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| PING_URL | The URL to ping | yes | |
| PING_STATUS_CODE_SUCCESS | The desired http status code | no | 200 |
| PING_TRIES | The number of failed attempts .ping will make before exiting. | no | 5 |
| PING_SLEEP_SECONDS | The number of seconds to wait betwen tries | no | 5 |

### .postman

Runs a postman collection.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| POSTMAN_ENVIRONMENT_UUID | The environment UUID; can be obtained from the [Postman API](https://community.postman.com/t/where-do-you-see-your-own-uid-for-collection/3537) | yes | |
| POSTMAN_COLLECTION_UUID | The collection UUID; can be obtained from the [Postman API](https://community.postman.com/t/where-do-you-see-your-own-uid-for-collection/3537) | yes | |

### Security gates

### .semgrep-sast

Runs [semgrep](https://semgrep.dev/) static analysis report. Report artifact is exported to Gitlab.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |

### .secret_detection_default_branch

Runs [gitleaks](https://github.com/zricethezav/gitleaks) secret detection report. Report artifact is exported to Gitlab. Used specifically for jobs running on `$CI_DEFAULT_BRANCH`.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |

### .secret_detection

Runs [gitleaks](https://github.com/zricethezav/gitleaks) secret detection report. Use this template for any branch other than `$CI_DEFAULT_BRANCH` (for comparison purposes these have to be split into distinct jobs). Report artifact is exported to Gitlab.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
