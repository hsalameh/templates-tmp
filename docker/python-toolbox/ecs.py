#!/usr/bin/env python3
import boto3
import os
import yaml
import json
from botocore.config import Config

my_config = Config(region_name=os.getenv("AWS_REGION"))


ecs_client = boto3.client("ecs", config=my_config)
region_name = ecs_client.meta.region_name
account = boto3.client("sts").get_caller_identity().get("Account")

with open(os.getenv("ECS_FILE")) as fh:
    configTemp = fh.read()
    configTemp = configTemp.replace("${region}", region_name)
    configTemp = configTemp.replace("${account}", account)
    configTemp = configTemp.replace("${enviroment}", os.getenv("ENVIRONMENT"))
    configTemp = configTemp.replace("${environment}", os.getenv("ENVIRONMENT"))
    configTemp = configTemp.replace("${commit}", os.getenv("CI_COMMIT_SHA"))
    configTemp = configTemp.replace("dev", os.getenv("ENVIRONMENT"))
    configECS = yaml.load(configTemp, Loader=yaml.FullLoader)
    configECS["ecs_service"] = os.getenv(
        "ENVIRONMENT") + "-" + configECS["name"] + "-service"
    configECS["cluster"] = os.getenv("ENVIRONMENT") + "-" + configECS["name"]
    configECS["environment"] = os.getenv("ENVIRONMENT")


def find_cluster():
    response = ecs_client.list_clusters()
    for cluster in response["clusterArns"]:
        if configECS["cluster"] == cluster.split("/")[1]:
            return True
    return False


def find_service():
    response = ecs_client.list_services(cluster=configECS["cluster"])
    for service in response["serviceArns"]:
        if configECS["ecs_service"] == service.split("/")[2]:
            return True
    return False


def get_current_task_definition():
    task = ecs_client.describe_services(
        cluster=configECS["cluster"], services=[configECS["ecs_service"]]
    )["services"][0]["deployments"][0]["taskDefinition"]
    task_data = ecs_client.describe_task_definition(
        taskDefinition=task, include=["TAGS"]
    )
    if task_data:
        return task_data
    return False


def build_tags():
    return (
        "service:"
        + configECS["name"]
        + ",environment:"
        + configECS["environment"]
        + ",region:"
        + region_name
        + ",version:"
        + str(os.getenv("CI_COMMIT_SHA"))
    )


def build_container_definitions(taskData):
    dd_tags = build_tags()
    containerDefinitions = []
    containerPorts = []
    environments = []
    secrets = []
    if "container" in configECS:
        main = {"name": "main-container", "essential": True}
        main["name"] = (
            configECS["environment"] + "-" +
            configECS["name"] + "-main-container"
        )
        main["image"] = os.getenv("ECR_REPOSITORY") + \
            ":" + os.getenv("CI_COMMIT_SHA")

        for ports in configECS["container"]["ports"]:
            for key, value in ports.items():
                var = {"hostPort": key, "protocol": value, "containerPort": key}
                containerPorts.append(var)
        main["portMappings"] = containerPorts

        if 'variables' in configECS["container"]:
            for variable in configECS["container"]["variables"]:
                for key, value in variable.items():
                    var = {"name": key, "value": value}
                    environments.append(var)
            main["environment"] = environments

        if 'secrets' in configECS["container"]:
            for secret in configECS["container"]["secrets"]:
                for key, value in secret.items():
                    sec = {"name": key, "valueFrom": value}
                    secrets.append(sec)
            main["secrets"] = secrets

        if bool(configECS["datadog"]["enabled"]):
            datadog = {
                "name": configECS["environment"] + "-" + configECS["name"] + "-datadog-agent",
                "image": "gcr.io/datadoghq/agent:latest",
                "portMappings": [
                    {"containerPort": 8125, "hostPort": 8125, "protocol": "udp"},
                    {"containerPort": 8126, "hostPort": 8126, "protocol": "tcp"},
                ],
                "essential": True,
                "environment": [
                    {"name": "ECS_FARGATE", "value": "true"},
                    {"name": "DD_SITE", "value": "datadoghq.com"},
                    {"name": "DD_TAGS", "value": dd_tags},
                ],
                "secrets": [{"name": "DD_API_KEY", "valueFrom": ""}],
                "logConfiguration": {
                    "logDriver": "awslogs",
                    "options": {
                        "awslogs-group": "/" + configECS["environment"] + "-" + configECS["name"] + "/datadog-agent",
                        "awslogs-region": "",
                        "awslogs-stream-prefix": "ecs",
                        "awslogs-create-group": "true",
                    },
                },
            }
            datadog["secrets"][0]["valueFrom"] = configECS["datadog"]["dd_api_key_arn"]
            datadog["logConfiguration"]["options"]["awslogs-region"] = region_name
            containerDefinitions.append(datadog)
            environments.append(
                {"name": "DD_ENV", "value": configECS["environment"]})
            environments.append(
                {"name": "DD_SERVICE", "value": configECS["name"]})
            if bool(configECS["datadog"]["log"]):
                datadog_log = {
                    "name": configECS["environment"] + "-" + configECS["name"] + "-log-router",
                    "image": "public.ecr.aws/aws-observability/aws-for-fluent-bit:2.23.4",
                    "essential": True,
                    "user": "0",
                    "logConfiguration": {
                        "logDriver": "awslogs",
                        "options": {
                            "awslogs-create-group": "true",
                            "awslogs-group": "/" + configECS["environment"] + "-" + configECS["name"] + "/firelens-container",
                            "awslogs-region": "us-east-1",
                            "awslogs-stream-prefix": "firelens",
                        },
                    },
                    "firelensConfiguration": {
                        "type": "fluentbit",
                        "options": {
                            "config-file-type": "file",
                            "config-file-value": "/fluent-bit/configs/parse-json.conf",
                            "enable-ecs-log-metadata": "true",
                        },
                    },
                }
                datadog_log["logConfiguration"]["options"][
                    "awslogs-region"
                ] = region_name
                containerDefinitions.append(datadog_log)
                main_log = {
                    "logConfiguration": {
                        "logDriver": "awsfirelens",
                        "options": {
                            "Format": "json",
                            "Host": "http-intake.logs.datadoghq.com",
                            "Match": "*",
                            "Name": "datadog",
                            "TLS": "on",
                            "compress": "gzip",
                            "dd_message_key": "log",
                            "dd_service": configECS["name"],
                            "dd_source": "fargate",
                            "dd_tags": dd_tags,
                            "provider": "ecs",
                        },
                        "secretOptions": [{"name": "apikey", "valueFrom": ""}],
                    }
                }
                main_log["logConfiguration"]["secretOptions"][0][
                    "valueFrom"
                ] = configECS["datadog"]["dd_api_key_arn"]
                main.update(main_log)

        containerDefinitions.append(main)
        return containerDefinitions
    else:
        return taskData["taskDefinition"]["containerDefinitions"]


def new_task_definition(taskData):
    if "roles" in configECS and "parameterstore" in configECS['roles'] and bool(configECS['roles']['parameterstore']):
        ssm_client = boto3.client('ssm', config=my_config)
        data_task_role = ssm_client.get_parameter(
            Name=configECS["environment"] + "-" + configECS["name"] +
            "-" + region_name + "-tasK_execution_role_arn",
            WithDecryption=True
        )
        data_task_exec_role = ssm_client.get_parameter(
            Name=configECS["environment"] + "-" + configECS["name"] +
            "-" + region_name + "-tasK_role_arn",
            WithDecryption=True
        )
        task_role = data_task_role['Parameter']['Value']
        task_exec_role = data_task_exec_role['Parameter']['Value']
    else:
        task_role = taskData["taskDefinition"]["taskRoleArn"]
        task_exec_role = taskData["taskDefinition"]["executionRoleArn"]

    new_task_definition = ecs_client.register_task_definition(
        containerDefinitions=build_container_definitions(taskData),
        family=taskData["taskDefinition"]["taskDefinitionArn"]
        .split("/")[1]
        .split(":")[0],
        taskRoleArn=task_role,
        executionRoleArn=task_exec_role,
        networkMode=taskData["taskDefinition"]["networkMode"],
        requiresCompatibilities=["FARGATE"],
        cpu=configECS["cpu"],
        memory=configECS["memory"],
        tags=[
            {
                "key": "COMMIT",
                "value": os.getenv("CI_COMMIT_SHA")
            },
            {
                "key": "CREATED_BY",
                "value": os.getenv("CI_COMMIT_AUTHOR").split("<")[1].split(">")[0],
            },
            {
                "key": "PIPELINE",
                "value": os.getenv("CI_PIPELINE_ID")
            },
            {
                "key": "ENVIRONMENT",
                "value": configECS["environment"]
            },
            {
                "key": "SERVICE",
                "value": configECS["name"]
            },
            {
                "key": "REGION",
                "value": region_name
            }

        ],
    )
    if new_task_definition["ResponseMetadata"]["HTTPStatusCode"] == 200:
        print("New task definition was created by you.....")
        print(".......")
        print(new_task_definition["taskDefinition"])
        if len(taskData["tags"]) == 0:
            commit = "Not found"
            author = "Not found"
        else:
            commit = taskData["tags"][1]["value"]
            author = taskData["tags"][0]["value"]
        output = {
            "green": {
                "task_definition": taskData["taskDefinition"]["taskDefinitionArn"],
                "commit": commit,
                "author": author,
            },
            "blue": {
                "task_definition": new_task_definition["taskDefinition"][
                    "taskDefinitionArn"
                ],
                "commit": os.getenv("CI_COMMIT_SHA"),
                "author": os.getenv("CI_COMMIT_AUTHOR").split("<")[1].split(">")[0],
            },
        }

        file_object = open("history.json", "w")
        file_object.write(json.dumps(output))
        file_object.close()
    else:
        print("error at creating new task register")
        exit(1)


def deploy():
    f = open('history.json')
    history = json.load(f)
    new_version = history['blue']['task_definition'].split("/")[1]
    old_version = history['green']['task_definition'].split("/")[1]
    response = ecs_client.update_service(
        cluster=configECS["cluster"],
        service=configECS["ecs_service"],
        taskDefinition=new_version,
        forceNewDeployment=True
    )
    if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
        print("------")
        print("Changed your taskDefinition from " + old_version +
              " to " + new_version + " with success.")
        print("Waiting this new release to be completed.")


def rollback():
    f = open('history.json')
    history = json.load(f)
    new_version = history['blue']['task_definition'].split("/")[1]
    old_version = history['green']['task_definition'].split("/")[1]
    response = ecs_client.update_service(
        cluster=configECS["cluster"],
        service=configECS["ecs_service"],
        taskDefinition=old_version,
        forceNewDeployment=True
    )
    if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
        print("------")
        print("Changed your taskDefinition from " + new_version +
              " to " + old_version + " with success.")
        print("Waiting this new release to be completed.")


# main app
if os.getenv("ACTION") == "task":
    if find_cluster() and find_service():
        task_definition = get_current_task_definition()
        new_task_definition(taskData=task_definition)
    else:
        print("Don't found your cluster/service")


if os.getenv("ACTION") == "deploy":
    if find_cluster() and find_service():
        deploy()
    else:
        print("Don't found your cluster/service")

if os.getenv("ACTION") == "rollback":
    if find_cluster() and find_service():
        rollback()
    else:
        print("Don't found your cluster/service")
