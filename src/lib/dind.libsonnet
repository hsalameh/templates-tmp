{
  DindJob: {
    image: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/docker:19.03.12",
    services: ["docker:19.03.12-dind"],
    variables: {
      DOCKER_HOST: "tcp://docker:2375",
      # This instructs Docker not to start over TLS.
      DOCKER_DRIVER: "overlay2",
      DOCKER_TLS_CERTDIR: ""
    }
  },
  KanikoBuild: {
    variables: {
      DOCKER_TAG: "$CI_COMMIT_SHA",
    },
    image: {
      name: "gcr.io/kaniko-project/executor:debug",
      entrypoint: [""]
    }
  },
}