local boilerplate = importstr "lib/boilerplate.txt";
local dind = import 'lib/dind.libsonnet';
local variables = {
  variables: {
    CODE_QUALITY_FORMAT: "json"
  },
};
local job_templates = {
  ".code-quality": dind.DindJob {
    stage: "test",
    cache: {},
    variables+: {
      CODE_QUALITY_IMAGE: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/codeclimate:0.85.24",
      CODECLIMATE_PREFIX: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/",
      CODE_QUALITY_REPORT_PATH: "$CI_PROJECT_DIR/gl-code-quality-report.$CODE_QUALITY_FORMAT"
    },
    needs: [],
    script: |||
      docker pull $CODE_QUALITY_IMAGE
      docker run \
      --env CODECLIMATE_CODE="$PWD" \
      --env CODECLIMATE_PREFIX="$CODECLIMATE_PREFIX" \
      --volume "$PWD":/code \
      --volume /var/run/docker.sock:/var/run/docker.sock \
      --volume /tmp/cc:/tmp/cc \
      $CODE_QUALITY_IMAGE analyze -f $CODE_QUALITY_FORMAT > $CODE_QUALITY_REPORT_PATH
    |||,
    dependencies: [],
    artifacts: {
      reports: {
        codequality: "$CODE_QUALITY_REPORT_PATH"
      },
      expire_in: "1 week"
    },
  }
};

local gitlabCiConf = variables + job_templates;
boilerplate + std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)