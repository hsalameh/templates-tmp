#!/usr/bin/env bash

yum update -y && yum install jq -y
aws --version
aws configure set aws_access_key_id ${AWS_ACCESS_KEY_ID}
aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY}
aws configure set region ${AWS_REGION}

previous_version=$(jq '.green.task_definition' history.json)
new_version=$(jq '.blue.task_definition' history.json)
echo "Changing from $previous_version to $new_version"
new_version=$(jq '.blue.task_definition' history.json | awk -F'/' '{print $2}' | sed 's/"//g')
aws ecs update-service --cluster ${CLUSTER_NAME} --service ${SERVICE_NAME} --force-new-deployment --region ${AWS_REGION} --task-definition $new_version
