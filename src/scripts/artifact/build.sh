#!/usr/bin/env bash

set -euo pipefail

# DIST_DIR <required>
# BUILD_DIR | ./
# BUILD_CMD | <required>
# ARTIFACT_NAME | COMMIT_SHORT_SHA.zip

build_env="build"
build_dir=${BUILD_DIR:-"./"}
envs="$DEV_ENV $QA_ENV $SANDBOX_ENV $PROD_ENV"
echo "dist: $DIST_DIR"

if [ -z "$BUILD_CMD" ]; then
	errcho "Required variable BUILD_CMD is not set. Exiting."
fi

function short_circuit_if_build_exists() {
	url="${GITLAB_REGISTRY_ARTIFACT_URL}/$build_env/$name"
	http_code=$(curl -H "$header_auth" -w "%{http_code}" -o/dev/null -sLO "$url")
	if [ "$http_code" -eq 200 ]; then
		echo "Artifact already exists"
		exit 0
	fi
}

short_circuit_if_build_exists

artifact_bundle=""
for env in $envs; do
	cd "${CI_PROJECT_DIR}/${build_dir}"
	env_varname="ENV_$env"
	# Checks if variable ENV_$env (e.g. ENV_dev/ENV_qa/etc) is set
	# If it is, it assumes this is an environment file that it can load
	# shellcheck disable=SC1090
	if [ -n "${!env_varname:-}" ]; then
		# shellcheck disable=SC1090
		. ${!env_varname}
	fi
	NODE_ENV=$env $BUILD_CMD
	mkdir artifact-contents/
	cp -r ${CI_PROJECT_DIR}/${DIST_DIR}/* artifact-contents
	tar -czvf "$env.tar.gz" artifact-contents
	rm -rf artifact-contents
	artifact_bundle="$artifact_bundle ${env}.tar.gz"
done

zip -r "$name" ${artifact_bundle}
upload_artifact "$name" "$build_env"
