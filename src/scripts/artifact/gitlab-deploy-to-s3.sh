#!/usr/bin/env bash

set -euxo pipefail

# S3_BUCKET | CI_ENVIRONMENT_NAME
# BUILD_ENV | CI_ENVIRONMENT_NAME
# ARTIFACT_NAME | CI_COMMIT_SHORT_SHA

check_deploy_window

build_environment="${BUILD_ENV:-$CI_ENVIRONMENT_NAME}"
deploy_s3_bucket="${S3_BUCKET:-$CI_ENVIRONMENT_NAME}"
name=${ARTIFACT_NAME:-"${CI_COMMIT_SHORT_SHA}.zip"}

if [[ -z ${deploy_s3_bucket-} ]]; then
	if [[ -z ${CI_ENVIRONMENT_URL-} ]]; then
		echo "ERROR: missing deploy_s3_bucket. No CI_ENVIRONMENT_URL to default to"
		exit 1
	fi
	echo "WARNING: missing deploy_s3_bucket; defaulting to CI_ENVIRONMENT_URL: $CI_ENVIRONMENT_URL"
	deploy_s3_bucket=$(echo "$CI_ENVIRONMENT_URL" | sed 's/https:\/\///')
fi
if [[ -z ${build_environment-} ]]; then
	if [[ -z ${CI_ENVIRONMENT_NAME-} ]]; then
		echo "ERROR: missing build_environment. No CI_ENVIRONMENT_NAME to default to"
		exit 1
	fi
	echo "WARNING: missing build environment; defaulting to CI_ENVIRONMENT_NAME: $CI_ENVIRONMENT_NAME"
	build_environment=$CI_ENVIRONMENT_NAME
fi

download_artifact "${build_environment}" "${name}"
unzip "${name}"
tar -xzvf "${build_environment}.tar.gz"

if [[ ${CLIENT} != "greatwall" ]]; then
	echo "CLIENT is $CLIENT"
	stat client-accounts-map.json &>/dev/null || aws secretsmanager get-secret-value --secret-id bitbucket/awsaccess/clients | jq '.SecretString | fromjson' >client-accounts-map.json
	account_id=$(jq -r --arg client "${CLIENT}" '.[$client]' client-accounts-map.json)
	aws configure set profile.deploy.role_arn arn:aws:iam::"${account_id}":role/OrganizationAccountAccessRole
	aws configure set profile.deploy.source_profile default
	aws configure set default.aws_access_key_id "${AWS_ACCESS_KEY_ID}"
	aws configure set default.aws_secret_access_key "${AWS_SECRET_ACCESS_KEY}"
	export AWS_PROFILE=deploy
	export AWS_SDK_LOAD_CONFIG="1"
	aws s3 sync --profile deploy artifact-contents "s3://${deploy_s3_bucket}" --exact-timestamps --delete --acl public-read
else
	aws s3 sync artifact-contents "s3://${deploy_s3_bucket}" --exact-timestamps --delete --acl public-read
fi

echo "Updated ${deploy_s3_bucket} with artifact ${name}"
