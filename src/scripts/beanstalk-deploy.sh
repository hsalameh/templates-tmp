#!/usr/bin/env bash

set -eux

if [[ ${CLIENT} != "greatwall" ]]; then
	echo "CLIENT is $CLIENT"
	stat client-accounts-map.json &>/dev/null || aws secretsmanager get-secret-value --secret-id bitbucket/awsaccess/clients | jq '.SecretString | fromjson' >client-accounts-map.json
	account_id=$(jq -r --arg client "${CLIENT}" '.[$client]' client-accounts-map.json)
	aws configure set profile.deploy.role_arn arn:aws:iam::"${account_id}":role/OrganizationAccountAccessRole
	aws configure set profile.deploy.source_profile default
	aws configure set default.aws_access_key_id "${AWS_ACCESS_KEY_ID}"
	aws configure set default.aws_secret_access_key "${AWS_SECRET_ACCESS_KEY}"
	export AWS_PROFILE=deploy
	export AWS_SDK_LOAD_CONFIG="1"
	source $ENV_FILE
	aws s3 cp --profile deploy application.zip s3://$S3_BUCKET/$ENVIRONMENT_NAME-$CI_PIPELINE_ID.zip
	aws elasticbeanstalk create-application-version --application-name $APPLICATION_NAME --version-label $ENVIRONMENT_NAME-$CI_PIPELINE_ID --source-bundle S3Bucket=$S3_BUCKET,S3Key=$ENVIRONMENT_NAME-$CI_PIPELINE_ID.zip --profile deploy
	aws elasticbeanstalk update-environment --application-name $APPLICATION_NAME --environment-name $ENVIRONMENT_NAME --version-label $ENVIRONMENT_NAME-$CI_PIPELINE_ID --profile deploy

else
	source $ENV_FILE
	aws s3 cp application.zip s3://$S3_BUCKET/$ENVIRONMENT_NAME-$CI_PIPELINE_ID.zip
	aws elasticbeanstalk create-application-version --application-name $APPLICATION_NAME --version-label $ENVIRONMENT_NAME-$CI_PIPELINE_ID --source-bundle S3Bucket=$S3_BUCKET,S3Key=$ENVIRONMENT_NAME-$CI_PIPELINE_ID.zip
	aws elasticbeanstalk update-environment --application-name $APPLICATION_NAME --environment-name $ENVIRONMENT_NAME --version-label $ENVIRONMENT_NAME-$CI_PIPELINE_ID
fi
