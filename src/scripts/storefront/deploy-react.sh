#!/usr/bin/env bash

set -euxo pipefail

set_aws_creds() {
	TARGET_AWS_ACC=$1
	aws configure set profile.deploy.role_arn "arn:aws:iam::${TARGET_AWS_ACC}:role/OrganizationAccountAccessRole"
	aws configure set profile.deploy.role_arn arn:aws:iam::201787177692:role/OrganizationAccountAccessRole
	aws configure set profile.deploy.source_profile default
	aws configure set default.aws_access_key_id "${AWS_ACCESS_KEY_ID}"
	aws configure set default.aws_secret_access_key "${AWS_SECRET_ACCESS_KEY}"
	export AWS_PROFILE=deploy
	export AWS_SDK_LOAD_CONFIG="1"
	export STOREFRONT_REF="${STOREFRONT_NAME}"
}

AWS_DEFAULT_REGION=${AWS_SELECTED_REGION:-"us-east-1"}
CLOUDFRONT_ID=${CLOUDFRONT_ID:-""}
PNI_DOCKERFILE_PREFIX=${PNI_DOCKERFILE_PREFIX:-""}
BUILD_ID_PREPEND_ENV_SLUG=${BUILD_ID_PREPEND_ENV_SLUG:-"1"}
STOREFRONT_ENVIRONMENT="${STOREFRONT_ENVIRONMENT:-"$CI_ENVIRONMENT_SLUG"}"

build_id="${BUILD_ID:-"$CI_COMMIT_SHORT_SHA"}"
if [[ -n ${PNI_DOCKERFILE_PREFIX} ]]; then
	build_id="${PNI_DOCKERFILE_PREFIX}-${STOREFRONT_ENVIRONMENT}"
fi
if [[ -n ${BUILD_ID_PREPEND_ENV_SLUG} ]]; then
	build_id="${STOREFRONT_ENVIRONMENT}-${build_id}"
fi

if [[ -n $PNI_DOCKERFILE_PREFIX ]]; then
	YQ_REF_FILE="${STOREFRONT_ENVIRONMENT}-${PNI_DOCKERFILE_PREFIX}-values.yaml"
	SF_NAME_REF="${STOREFRONT_NAME}-${PNI_DOCKERFILE_PREFIX}-${STOREFRONT_ENVIRONMENT}"
else
	YQ_REF_FILE="${STOREFRONT_ENVIRONMENT}-values.yaml"
	SF_NAME_REF="${STOREFRONT_NAME}-${STOREFRONT_ENVIRONMENT}"
fi

echo $build_id
export build_id
#echo "http://dl-4.alpinelinux.org/alpine/edge/community" >>/etc/apk/repositories
rm -rf /var/cache/apk/* && rm -rf /tmp/*
apk update
apk add git yq --no-cache
git clone https://storefront-deployments:$STOREFRONT_GITPASS@gitlab.com/fabric2/km/storefront/helm.git
git config user.email "ops@fabric.inc"
git config user.name "ops"
cd helm
git checkout main

yq eval '.deployment.tag = env(build_id)' ${STOREFRONT_NAME}/${YQ_REF_FILE} >>"${build_id}.yml"
mv "${build_id}.yml" "${STOREFRONT_NAME}/${YQ_REF_FILE}"
cat "${STOREFRONT_NAME}/${YQ_REF_FILE}"

if [[ ${AWS_DEFAULT_REGION} == "us-east-2" && ${STOREFRONT_ENVIRONMENT} == "production" ]]; then
	set_aws_creds "201787177692"
	aws eks --region ${AWS_DEFAULT_REGION} update-kubeconfig --name ${EKS_CLUSTER_NAME_DR} --profile deploy --role-arn arn:aws:iam::201787177692:role/OrganizationAccountAccessRole
	CERT_ARN="$(aws --profile deploy --region "${AWS_DEFAULT_REGION}" secretsmanager get-secret-value --secret-id storefront/production/"${AWS_DEFAULT_REGION}"/secrets | jq -r '.SecretString' | jq -r --arg sclient "$STOREFRONT_REF" 'to_entries[] |select(.key == $sclient) |.value')"
	helm upgrade ${SF_NAME_REF} ${STOREFRONT_NAME} --install --values ${STOREFRONT_NAME}/${YQ_REF_FILE} --set ingress.certificate=${CERT_ARN} -n ${STOREFRONT_ENVIRONMENT}
else
	aws --profile default configure set aws_access_key_id ${EKS_ACCESS_ID}
	# shellcheck disable=SC2153
	aws --profile default configure set aws_secret_access_key ${EKS_SECRET_ACCESS_KEY}
	CREDENTIALS=$(aws sts assume-role --role-arn $EKS_KUBECTL_ROLE_ARN --role-session-name "${CI_PROJECT_NAME}-${GITLAB_USER_LOGIN}" --duration-seconds 900)
	AWS_ACCESS_KEY_ID="$(echo ${CREDENTIALS} | jq -r '.Credentials.AccessKeyId')"
	AWS_SECRET_ACCESS_KEY="$(echo ${CREDENTIALS} | jq -r '.Credentials.SecretAccessKey')"
	AWS_SESSION_TOKEN="$(echo ${CREDENTIALS} | jq -r '.Credentials.SessionToken')"
	AWS_EXPIRATION=$(echo ${CREDENTIALS} | jq -r '.Credentials.Expiration')
	export AWS_ACCESS_KEY_ID
	export AWS_SECRET_ACCESS_KEY
	export AWS_SESSION_TOKEN
	export AWS_EXPIRATION
	aws eks update-kubeconfig --name ${EKS_CLUSTER_NAME}
	helm upgrade ${SF_NAME_REF} ${STOREFRONT_NAME} --install --values ${STOREFRONT_NAME}/${YQ_REF_FILE} -n ${STOREFRONT_ENVIRONMENT}
fi

if [[ -z ${CLOUDFRONT_ID} ]]; then
	echo "No CF Distribution To Clean"
else
	echo "Clearing CloudFron Cache"
	aws cloudfront --region "${AWS_DEFAULT_REGION}" create-invalidation --distribution-id $CLOUDFRONT_ID --paths "/*"

fi

git config user.email "ops@fabric.inc"
git config user.name "ops"
CHANGES=$(git status --porcelain | wc -l)
if [[ ${CHANGES} -gt 0 ]]; then
	echo "Committing updated files to git"
	git add -A
	git commit -m "${STOREFRONT_NAME}-${STOREFRONT_ENVIRONMENT}:${build_id}"
	git push origin main
else
	echo "Nothing to commit"
fi
