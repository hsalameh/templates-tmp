#!/usr/bin/env bash

set -x

# This is used to authenticate in order to pull modules from the private Gitlab registry.
cat <<EOF >~/.terraformrc
credentials "gitlab.com" {
token = "$CI_JOB_TOKEN"
}
credentials "app.terraform.io" {
token = "${TF_CLOUD_TOKEN:-""}"
}
credentials "fabric.scalr.io" {
token = "${SCALR_REGISTRY_TOKEN:-""}"
}
EOF

state_name="$(echo $TF_STATE_NAME | sed 's/\//-/' | sed 's/\./-/g')"

TF_ADDRESS=https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/terraform/state/$state_name

# if using a project access token the username can be any non-empty string
# create an access token by going to the project settings -> access tokens
# personal access token can also be used.
terraform init \
	-backend-config=username=terraform-state \
	-backend-config=password="$TF_ACCESS_TOKEN" \
	-backend-config=address="$TF_ADDRESS" \
	-backend-config=lock_address="$TF_ADDRESS/lock" \
	-backend-config=unlock_address="$TF_ADDRESS/lock" \
	-backend-config=lock_method=POST \
	-backend-config=unlock_method=DELETE \
	-backend-config=retry_wait_min=5 \
	-reconfigure
