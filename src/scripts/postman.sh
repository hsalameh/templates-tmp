#!/usr/bin/env bash

set -ex

if [ ! -z "$POSTMAN_COLLECTION_PATH" ]; then
	collection="$POSTMAN_COLLECTION_PATH"
elif [ ! -z "$POSTMAN_COLLECTION_UUID" ]; then
	collection="https://api.getpostman.com/collections/$POSTMAN_COLLECTION_UUID?apikey=$POSTMAN_API_KEY"
else
	echo "No collection specified!"
	exit 1
fi

if [ ! -z "$POSTMAN_ENVIRONMENT_PATH" ]; then
	environment="$POSTMAN_ENVIRONMENT_PATH"
elif [ ! -z "$POSTMAN_ENVIRONMENT_UUID" ]; then
	environment="https://api.getpostman.com/environments/$POSTMAN_ENVIRONMENT_UUID?apikey=$POSTMAN_API_KEY"
else
	echo "No environment specified!"
	exit 1
fi

newman run ${collection} -e ${environment} --reporters cli,junit --reporter-junit-export report.xml
