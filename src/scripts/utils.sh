#!/usr/bin/env bash

export name=${ARTIFACT_NAME:-"${CI_COMMIT_SHORT_SHA}.zip"}
export header_auth="${GITLAB_REGISTRY_TOKEN_HTTP_HEADER}:${GITLAB_REGISTRY_TOKEN_VALUE}"

red="31;1m"
magenta="35;1m"
cyan="36;1m"
nc="0;m"

function errcho() {
	msg="$1"
	echo -e "\033[${red}ERROR: ${msg}\033[${nc}"
	exit 1
}

function loginfo() {
	msg="$1"
	echo -e "\033[${magenta}INFO: ${msg}\033[${nc}"
}

function check_deploy_window() {
	if [[ -n ${FF_DEPLOY_WINDOW:-} ]]; then
		can_deploy=$(curl \
			--silent \
			--location \
			--header "x-api-key:${PLATFORM_API_KEY}" \
			--header "Content-Type:application/json" \
			"${PLATFORM_BASE_URL}/timeslot/check-availablity" | jq '.can_deploy')

		if [[ ${can_deploy} == "false" ]]; then
			errcho "Failed to deploy: deployment is outside of the deploy window. Exiting."
		fi
	fi
}

function get_previous_env() {
	curr_env="$1"
	# shellcheck disable=SC2206
	envs=("build" $DEV_ENV $QA_ENV $SANDBOX_ENV $PROD_ENV)
	for i in "${!envs[@]}"; do
		if [[ ${envs[i]} == "$curr_env" ]]; then
			if [[ $i -eq 0 ]]; then
				errcho '"build" is not a valid deployment environment'
			fi
			echo "${envs[$((i - 1))]}"
			return
		fi
	done
	errcho "build environment \"$curr_env\" not found"
}

function upload_artifact() {
	name_and_ext="$1"
	env="$2"
	url="${GITLAB_REGISTRY_ARTIFACT_URL}/$env/$name_and_ext"
	loginfo "uploading ${name_and_ext} to ${env}"
	http_code=$(curl -H "$header_auth" -X PUT -w "%{http_code}" -o /dev/null -s --data-binary "@${name_and_ext}" "$url")
	if [ "${http_code}" -ne 201 ]; then
		errcho "upload failed: response code ${http_code}"
	fi
}

function download_artifact() {
	env="$1"
	name_and_ext="$2"
	loginfo "downloading ${name_and_ext} from ${env}"
	http_code=$(curl -H "$header_auth" -w "%{http_code}" -sLO "${GITLAB_REGISTRY_ARTIFACT_URL}/${env}/${name_and_ext}")
	if [ "${http_code}" -eq 404 ]; then
		loginfo "promoting ${name_and_ext} from $(get_previous_env "${env}") to ${env}"
		promote_and_download_artifact "${name_and_ext}" "$(get_previous_env "${env}")" "${env}"
	fi
}

function promote_and_download_artifact() {
	name_and_ext="$1"
	prev_env="$2"
	curr_env="$3"
	http_code=$(curl -H "$header_auth" -w "%{http_code}" -sLO "${GITLAB_REGISTRY_ARTIFACT_URL}/${prev_env}/${name_and_ext}")
	if [ "${http_code}" -eq 404 ]; then
		errcho "unable to download artifact $name_and_ext: artifact not found."
	elif [ "${http_code}" -ne 200 ]; then
		errcho "unable to download artifact $name_and_ext: http code: ${http_code}"
	fi
	http_code=$(upload_artifact "${name_and_ext}" "${curr_env}")
	if [ "${http_code}" -ne 201 ]; then
		errcho "unable to upload artifact ${name_and_ext}: http code: ${http_code}"
	fi
}
