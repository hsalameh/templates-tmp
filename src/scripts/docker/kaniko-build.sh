#!/usr/bin/env bash

mkdir -p /kaniko/.docker
echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" >/kaniko/.docker/config.json

/kaniko/executor --cache=true \
	--context "${CONTEXT}" \
	--dockerfile "${DOCKERFILE_PATH}" \
	--destination "${DOCKER_IMAGE}" \
	--destination "${DOCKER_REGISTRY}:latest" \
	--label org.opencontainers.image.vendor=$CI_SERVER_URL/$GITLAB_USER_LOGIN \
	--label org.opencontainers.image.authors=$CI_SERVER_URL/$GITLAB_USER_LOGIN \
	--label org.opencontainers.image.commit=$CI_COMMIT_SHA \
	--label org.opencontainers.image.revision=${DOCKER_TAG} \
	--label org.opencontainers.image.source=$CI_PROJECT_URL \
	--label org.opencontainers.image.documentation=$CI_PROJECT_URL \
	--label org.opencontainers.image.licenses=$CI_PROJECT_URL \
	--label org.opencontainers.image.url=$CI_PROJECT_URL \
	--label vcs-url=$CI_PROJECT_URL \
	--label com.gitlab.ci.user=$CI_SERVER_URL/$GITLAB_USER_LOGIN \
	--label com.gitlab.ci.email=$GITLAB_USER_EMAIL
