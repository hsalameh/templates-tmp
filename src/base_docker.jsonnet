local boilerplate = importstr "lib/boilerplate.txt";
local dind = import "lib/dind.libsonnet";

local global_vars = {
  variables: {
    IMAGE: "$CI_REGISTRY_IMAGE:latest",
    DEPLOYMENT_ROLE: "arn:aws:iam::${ACCOUNT_NUMBER}:role/OrganizationAccountAccessRole"
  },
};
local job_templates = {
  ".dind": dind.DindJob,
  ".deployment_script": {
    image: {
      name: "registry.gitlab.com/rafidsaad/images/aws-docker:latest",
      entrypoint: [""],
    },
    before_script: importstr "scripts/docker/deployment.sh"
  },
  ".build_and_push_gitlab": dind.DindJob {
    script: importstr "scripts/docker/build-and-push.sh"
  },
  ".docker_build_kaniko": dind.KanikoBuild{
    variables: {
      DOCKERFILE_PATH: "$CI_PROJECT_DIR/Dockerfile",
      DOCKER_TAG: "$CI_COMMIT_SHA",
      CONTEXT: "$CI_PROJECT_DIR",
      DOCKER_REGISTRY: "$CI_REGISTRY_IMAGE",
      DOCKER_IMAGE: "$DOCKER_REGISTRY:$DOCKER_TAG",
    },
    script: importstr "scripts/docker/kaniko-build.sh"
  },
  ".docker_push_gcrane": {
    image: "amazon/aws-cli",
    variables: {
      DOCKER_TAG: "$CI_COMMIT_SHA",
      AWS_REGION: "us-east-1",
      DOCKER_REGISTRY: "$CI_REGISTRY_IMAGE",
      DOCKER_IMAGE: "$DOCKER_REGISTRY:$DOCKER_TAG",
    },
    script: importstr "scripts/docker/gcrane-copy.sh"
  },
};

local gitlabCiConf =  global_vars +
                      job_templates;

boilerplate + std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)
