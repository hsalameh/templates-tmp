
local boilerplate = importstr "lib/boilerplate.txt";

local stages = {
  stages: []
};

local global_vars = {
  variables: {
  },
};
local job_templates = {
  ".get-clients": {
    image: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/python-toolbox:v0.0.1",
    variables: {
      ENV_NAME: "",
      STAGE: "",
      PREVIOUS_STAGE: ""
    },
    script: importstr "scripts/platform/get-mapping.sh",
    artifacts: {
      paths: [
        "generated-config.yml",
        "clients.txt"
      ],
    },
  },
};

local gitlabCiConf =  stages +
                      global_vars +
                      job_templates;
boilerplate + std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)
