local boilerplate = importstr "lib/boilerplate.txt";
local variables = {
  variables: {
    SECURE_ANALYZERS_PREFIX: "registry.gitlab.com/gitlab-org/security-products/analyzers",
    SECRETS_ANALYZER_VERSION: "3",
    SECRET_DETECTION_EXCLUDED_PATHS: ""
  }
};
local job_templates = {
  ".secret-analyzer": {
    stage: "test",
    image: "$SECURE_ANALYZERS_PREFIX/secrets:$SECRETS_ANALYZER_VERSION",
    artifacts: {
      reports: {
        secret_detection: "gl-secret-detection-report.json"
      },
    },
    services: [],
  },
  ".secret_detection_default_branch": {
    extends: ".secret-analyzer",
    script: "/analyzer run"
  },
  ".secret_detection": {
    extends: ".secret-analyzer",
    script: |||
      if [[ $CI_COMMIT_TAG ]]; then echo "Skipping Secret Detection for tags. No code changes have occurred."; exit 0; fi
      git fetch origin $CI_DEFAULT_BRANCH $CI_COMMIT_REF_NAME
      git log --left-right --cherry-pick --pretty=format:"%H" refs/remotes/origin/$CI_DEFAULT_BRANCH...refs/remotes/origin/$CI_COMMIT_REF_NAME > "$CI_COMMIT_SHA"_commit_list.txt
      export SECRET_DETECTION_COMMITS_FILE="$CI_COMMIT_SHA"_commit_list.txt
      /analyzer run
      rm "$CI_COMMIT_SHA"_commit_list.txt
    |||
  },

};
local config = variables + job_templates;
boilerplate + std.manifestYamlDoc(config, indent_array_in_object=true)
