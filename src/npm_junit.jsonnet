local boilerplate = importstr "lib/boilerplate.txt";
local job_templates = {
  ".npm-junit": {
    image: "registry.gitlab.com/fabric2/ci-templates/node:10",
    needs: [],
    variables: {
      report_dir: "./",
      npm_dir: "./"
    },
    script: |||
      cd $npm_dir
      echo "===== Running $npm_cmd ====="
      npm run | grep -E "^\s+$npm_cmd$" || echo -e "===== $npm_cmd script not found! =====\nAdd \"npm run test\" to package.json scripts"
      npm run "$npm_cmd"
    |||,
    artifacts: {
      paths: [
        "client/junit.xml"
      ],
      reports: {
        junit: "client/junit.xml",
        coverage_report: {
          coverage_format: 'cobertura',
          path: 'cobertura-coverage.xml',
        },
      },
    },
   },
  };

boilerplate + std.manifestYamlDoc(job_templates, indent_array_in_object=true)
