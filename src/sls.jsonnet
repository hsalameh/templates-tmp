
local boilerplate = importstr "lib/boilerplate.txt";
local utils = import 'lib/utils.libsonnet';

local stages = {
  stages: ["sls"]
};

local global_vars = {
  variables: {
    BUILD_DIR: "./",
    PLATFORM_BASE_URL: "https://rc3jxwm5t6.execute-api.us-east-1.amazonaws.com/dev/v1/deployment",
    PLATFORM_DISABLE_HISTORY: "",
    BUILD_ID: "$CI_COMMIT_SHORT_SHA"
  },
};

local job_templates = {
  ".sls-node-10": {
    image: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/node-10-toolbox:v0.0.1"
  },
  ".sls-node-14": {
    image: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/node-14-toolbox:v0.0.3"
  },
  ".sls-webpack": {
    stage: "sls",
    variables: {
      STAGE: "",
    },
    before_script: "apt-get update && apt-get install -y p7zip",
    script: importstr "scripts/sls/webpack.sh",
  },
  ".sls-deploy": {
    stage: "sls",
    variables: {
      PLATFORM: "",
      PREVIOUS_STAGE: "",
      STAGE: "",
      CLIENT: "",
      PLATFORM_DISABLE_HISTORY: ""
    },
    before_script: "apt-get update && apt-get install -y p7zip",
    script: 
      utils.prepend_utils(
      (importstr "scripts/platform/add-job.sh") +
      (importstr "scripts/sls/deploy.sh") +
      (importstr "scripts/platform/update-job.sh")),
  },
  ".sls-webpack-10": {
    extends: [
      ".sls-webpack",
      ".sls-node-10"
    ],
  },
  ".sls-webpack-14": {
    extends: [
      ".sls-webpack",
      ".sls-node-14"
    ],
  },
  ".sls-deploy-10": {
    extends: [
      ".sls-deploy",
      ".sls-node-10"
    ],
  },
  ".sls-deploy-14": {
    extends: [
      ".sls-deploy",
      ".sls-node-14"
    ],
  },
};

local gitlabCiConf =  stages +
                      global_vars +
                      job_templates;

boilerplate + std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)
