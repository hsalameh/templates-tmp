local boilerplate = importstr "lib/boilerplate.txt";
local variables = {
  variables: {
    PLAN: "plan.tfplan",
    PLAN_JSON: "plan.json"
  },
};
local job_templates = {
  ".base-terraform": {
    image: {
      name: "registry.gitlab.com/fabric2-public/cicd/docker-images/terraform-1.2.3:0.2.0",
    },
    before_script: ["cd $TF_ROOT"]
  },
  ".terraform-init": {
    extends: ".base-terraform",
    stage: "terraform-init",
    script: importstr "scripts/terraform/init.sh",
    artifacts: {
      paths: [
        "**/.terraform",
        "**/.terraform.**"
      ],
    },
  },
  ".terraform-validate": {
    extends: ".base-terraform",
    stage: "terraform-validate",
    script: importstr "scripts/terraform/validate.sh",
  },
  ".terraform-plan": {
    extends: ".base-terraform",
    stage: "terraform-plan",
    before_script: [
      "cd $TF_ROOT"
    ],
    script: importstr "scripts/terraform/plan.sh",
    artifacts: {
      paths: [
        "**/.terraform",
        "**/.terraform.**",
        "**/*.tfplan",
      ],
      expire_in: "1 week"
    }
  },
  ".terraform-plan-apply": {
    extends: ".base-terraform",
    stage: "terraform-apply",
    before_script: [
      importstr "scripts/terraform/plan.sh"
    ],
    script: "terraform apply -auto-approve $PLAN",
    artifacts: {
      paths: [
        "$TF_ROOT/outputs/**/*",
        "$TF_ROOT/errored.tfstate"
      ],
    },
  },
  ".terraform-apply": {
    extends: ".base-terraform",
    stage: "terraform-apply",
    script: "terraform apply -auto-approve $PLAN",
    artifacts: {
      paths: [
        "$TF_ROOT/outputs/**/*",
        "$TF_ROOT/errored.tfstate"
      ],
    },
  },
  ".terraform-destroy": {
    extends: ".base-terraform",
    stage: "terraform-destroy",
    script: importstr "scripts/terraform/destroy.sh"
  },
};
local gitlab_config = variables + job_templates;
boilerplate + std.manifestYamlDoc(gitlab_config, indent_array_in_object=true)
