
local utils = import 'lib/utils.libsonnet';
local boilerplate = importstr "lib/boilerplate.txt";

local scripts = {
  deploy_storefront: importstr "scripts/storefront/deploy.sh"
};

local job_templates = {
  ".deploy_storefront": {
    image: "alpine/k8s:1.21.2",
    script: scripts.deploy_storefront
  },
};

boilerplate + std.manifestYamlDoc(job_templates, indent_array_in_object=true)
